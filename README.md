# README #

The CodeMules Unrest API provides RESTful oriented network functionality for interacting with RESTful web apis.

### What is this repository for? ###

* The CodeMules Unrest API provides RESTful oriented network functionality for interacting with RESTful web apis.
* 0.1.0

### How do I get set up? ###

* Grab a version of the Unrest package and install it into your Unity project

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Tom Guinther (codemules@gmail.com)