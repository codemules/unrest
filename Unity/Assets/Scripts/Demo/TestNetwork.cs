﻿using UnityEngine;

using System ;
using System.Text;
using CodeMules.Json;
using CodeMules.Unrest;

public class TestNetwork : MonoBehaviour {

	#region Fields
	#endregion

	#region Unity Callbacks
	/*
	// Use this for construtor style initilization
	void Awake() {
		
	}
	*/


    private Neo4jEnvoy envoy;

	// Use this for run-time initialization
	void Start ()
	{
	    envoy = new Neo4jEnvoy("Neo4j",new Uri("http://localhost:7474/"),"neo4j","dancer87");

	    envoy.GetServices()
	        .Always(
	            (promise) =>
	            {
	                if (promise.IsSuccessful)
	                {
	                    /*Debug.Log(networkPromise.Result.StatusMessage);

	                    string entity = networkPromise.Result.NetworkEntity.ToString();
	                    Debug.Log(entity);
                        */

	                    Debug.Log(promise.Result);
	                    Debug.Log(JsonFormatter.FormatJson(promise.Result));
	                }
	                else
	                {
	                    Debug.LogErrorFormat("Network Error: {0}",promise.Exception);
	                }
	            }
	        ).Then(
	            (services) => envoy.GetServiceData(services["node"].ToString())
	                .Always(
	                    (promise) =>
	                    {
	                        if (promise.IsSuccessful)
	                        {
	                            //Debug.Log(networkPromise.Result.StatusMessage);

                                //string entity = networkPromise.Result.NetworkEntity.ToString();
                                //Debug.Log(entity);


	                            Debug.Log(JsonFormatter.FormatJson(promise.Result));
	                        }
	                        else
	                        {
	                            Debug.LogErrorFormat("Network Error: {0}",promise.Exception);
	                        }
	                    }
	                ));

	    envoy.Send(
	        new Request("db/data/transaction/commit", HttpMethod.Post)
	            .WithContentType(HttpContentType.JsonContentType)
	            .WithBody( Encoding.UTF8.GetBytes(
    	                "{ \"statements\" : [ { \"statement\" : \"CREATE (n) RETURN id(n)\" } ] }"
	                    ))
	            .WithAccept(HttpContentType.JsonUtfContentType)
	    ).Always(
	        (promise) =>
	        {
	            if (promise.IsSuccessful)
	            {
	                Debug.Log(promise.Result.NetworkEntity.ToString());
	            }
	            else
	            {
	                Debug.LogErrorFormat("Network Error: {0}",promise.Exception);
	            }
	        }
	    );
	}


	/* 
	// Called when the behavior is enabled
	void OnEnable() {}
	*/

	/* 
	// Called when the behavior is disabled
	void OnDisable() {}
	*/

	/* 
	// Called when the behavior is enabled/disabled
	void OnDestroy() {}
	*/
	
	/*
	// Update is called once per frame
	void Update () {
		
	}
	*/
	#endregion

	#region Properties
	#endregion

	#region Methods
	#endregion

	#region Implementation
	#endregion

}
