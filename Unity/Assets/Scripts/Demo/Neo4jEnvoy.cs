﻿
using System ;
using System.Collections.Generic;
using CodeMules.Json;
using CodeMules.Promises;
using CodeMules.Unrest;
using CodeMules.Unrest.Unity;

public class Neo4jEnvoy : NetworkEnvoy {

	#region Fields

    private string basicAuth;
	#endregion

    public Neo4jEnvoy(string name,Uri baseUrl,string username,string password)
        : base(name,new UnityWebRequestNetwork("Neo4j"), baseUrl)
    {
        basicAuth = HttpUtils.GetBasicAuthPayload(username, password);
    }

	#region Properties
	#endregion

	#region Methods

    public IPromise<IDictionary<string,object>> GetServices()
    {
        Promise<IDictionary<string,object>> promise = new Promise<IDictionary<string, object>>();

        Send(
            Request.Get("db/data/").
                WithAccept(HttpContentType.JsonContentType)
            ).Always(
                (networkPromise) =>
                {
                    if (networkPromise.IsSuccessful)
                    {
                        promise.Deliver(JsonSerializer.Deserialize(networkPromise.Result.NetworkEntity.ToString()));
                    }
                    else
                    {
                        promise.Deliver(promise.Exception);
                    }
                });

        return promise;
    }

    public IPromise<IDictionary<string, object>> GetServiceData(string url)
    {
        Promise<IDictionary<string,object>> promise = new Promise<IDictionary<string, object>>();

        Send(
            Request.Get(url).
                WithAccept(HttpContentType.JsonContentType)
        ).Always(
            (networkPromise) =>
            {
                if (networkPromise.IsSuccessful)
                {
                    promise.Deliver(JsonSerializer.Deserialize(networkPromise.Result.NetworkEntity.ToString()));
                }
                else
                {
                    promise.Deliver(networkPromise.Exception);
                }
            });

        return promise;
    }

    #endregion

	#region Implementation
	#endregion

    protected override INetworkRequestController CreateNetworkRequest(IRequest request, Uri baseUrl)
    {
        INetworkRequest networkRequest = base.CreateNetworkRequest(
            request,
            baseUrl
            ).WithHeader(HttpHeader.Authorization,basicAuth);

        return networkRequest as INetworkRequestController;
    }
}
