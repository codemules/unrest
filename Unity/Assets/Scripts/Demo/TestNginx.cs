﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMules.Unrest;
using CodeMules.Unrest.Unity;

public class TestNginx : MonoBehaviour {

	// Use this for initialization
	void Start () {
		NetworkEnvoy ne = new NetworkEnvoy("nginx",new UnityWebRequestNetwork("nginx"),"http://localhost/") ;

		ne.Send(
			Request.Get("java")
		).Always(
			(promise) => {
				if (promise.IsSuccessful) {
					Debug.Log(promise.Result.NetworkEntity.ToString()) ;
				} else {
					Debug.LogError(promise.Exception);
				}
			}
		).Then(
			ne.Send(
				Request.Get("clojure")
			).Always(
				(promise) => {
					if (promise.IsSuccessful) {
						Debug.Log(promise.Result.NetworkEntity.ToString()) ;
					} else {
						Debug.LogError(promise.Exception);
					}
				}
			)
		);
	}
}
