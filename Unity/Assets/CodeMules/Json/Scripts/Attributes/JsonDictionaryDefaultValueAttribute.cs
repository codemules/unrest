﻿using System;
using System.Collections.Generic;

namespace CodeMules.Json {
	/// <summary>
	/// Json default value that generates a <c>Dictionary{string,object}</c>
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonDictionaryDefaultValueAttribute : JsonDefaultValueAttribute {
		/// <summary>
		/// Determine if the dictionary key lookup is case-sensitive
		/// </summary>
		/// <value>
		/// <c>true</c> if key lookup is case-sensitive, <c>false</c> if it is case-INsensitive
		/// </value>
		public bool CaseSensitive { get; set; }

		/// <summary>
		/// Generate a new <c>Dictionary{string,object}</c>
		/// </summary>
		/// <returns>a new <c>Dictionary{string,object}</c></returns>
		public override object GetDefaultValue() {
			return new Dictionary<string, object>(
				CaseSensitive ? StringComparer.InvariantCulture : StringComparer.InvariantCultureIgnoreCase
			);
		}
	}
}

