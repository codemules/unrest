using System;

namespace CodeMules.Json {
	/// <summary>
	/// Use this attribute to ignore a property for Json Encoding
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonIgnoreAttribute : Attribute {}
}

