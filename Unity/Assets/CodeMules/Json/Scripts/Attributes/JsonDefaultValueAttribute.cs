﻿using System;
namespace CodeMules.Json {
	/// <summary>
	/// A class used by Json deserialization to create default property values
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonDefaultValueAttribute  : Attribute {
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonDefaultValueAttribute"/> class.
		/// </summary>
		/// <remarks>
		/// Used by base classes that need to generate a default value dynamically via <see cref="GetDefaultValue()"/>
		/// </remarks>
		protected JsonDefaultValueAttribute() {}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonDefaultValueAttribute"/> class.
		/// </summary>
		/// <param name="defaultValue">The default value</param>
		public JsonDefaultValueAttribute(object defaultValue) {
			DefaultValue = defaultValue;
		}
		
		/// <summary>
		/// Get/Set the default value
		/// </summary>
		/// <value>The default value</value>
		private object DefaultValue { get; set; }

		/// <summary>
		/// Get the default value
		/// </summary>
		/// <returns>The default value</returns>
		public virtual object GetDefaultValue() {
			return DefaultValue;
		}
	}
}

