using System;

namespace CodeMules.Json {
	/// <summary>
	/// Use this attribute on a class to signify that Json 
	/// serialization should use the property serialization method
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class JsonSerializationAttribute : Attribute {}
}

