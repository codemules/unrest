﻿using System;

namespace CodeMules.Json {
	/// <summary>
	/// Identifies an implementation of an interface to use for
	/// deserialization
	/// </summary>
	[AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface|AttributeTargets.Property)]
	public class JsonDeserializeAsTypeAttribute : Attribute {
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonDeserializeAsAttribute"/> class.
		/// </summary>
		/// <param name="deserializeAs">The type to use for deserialization</param>
		public JsonDeserializeAsTypeAttribute(Type deserializeAs) {
			DeserializeAsType = deserializeAs;
		}

		/// <summary>
		/// Get the <see cref="Type"/> used for deserializing
		/// </summary>
		/// <value>The <see cref="Type"/> used for deserializing</value>
		public Type DeserializeAsType { get; private set; }
	}
}

