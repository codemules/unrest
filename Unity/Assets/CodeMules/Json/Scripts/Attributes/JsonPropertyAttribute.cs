using System;

namespace CodeMules.Json {
	/// <summary>
	/// Use this attribute to specify a key name for a Json serialized
	/// property value
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonPropertyAttribute : Attribute {
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonPropertyAttribute"/> class.
		/// </summary>
		/// <param name="key">The json property name/key</param>
		public JsonPropertyAttribute (string key) {
			// Validation.CheckString(key, "key");

			Key = key;
		}

		/// <summary>
		/// Get the Json property key
		/// </summary>
		/// <value>The key for the property</value>
		public string Key { get ; private set ; }
	}
}

