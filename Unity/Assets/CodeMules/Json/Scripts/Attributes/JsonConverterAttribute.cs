
using System;

namespace CodeMules.Json {
	/// <summary>
	/// Abstract attribute used to define custom Json conversion functionality
	/// by using derivation and overrides
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public abstract class JsonConverterAttribute : Attribute {
		/// <summary>
		/// Called when a value from Json representation should be converted appropriately
		/// for .NET representation
		/// </summary>
		/// <returns>the converted value</returns>
		/// <param name="value">The value to convert</param>
		public virtual object FromJson(object value) {
			return value;
		}
		
		/// <summary>
		/// Called when a value from a .NET representation should be converted to the
		/// appropriate Json representation
		/// </summary>
		/// <returns>The value to json encode</returns>
		/// <param name="value">the .NET representation of the value</param>
		public virtual object ToJson(object value) {
			return value;
		}
	}
	
}
