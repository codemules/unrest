using System;

namespace CodeMules.Json {
	/// <summary>
	/// Use this attribute to force a property value to be encoded
	/// as a string
	/// </summary>
	/// <remarks>
	/// Your deserialization must take into account that the property is
	/// encoded as a string
	/// </remarks>
	[AttributeUsage(AttributeTargets.Property)]
	public class JsonEncodeAsStringAttribute : Attribute {}
}

