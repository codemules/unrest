﻿using System;

namespace CodeMules.Json {
	/// <summary>
	/// Conversion of <see cref="DateTime"/> values using ticks (i.e., long)
	/// as the json representation
	/// </summary>
	/// <remarks>
	/// Converts a <see cref="DateTime"/> to/from a ticks (long) value
	/// </remarks>
	public class JsonDateTimeTicksConverterAttribute : JsonConverterAttribute {
		/// <summary>
		/// Convert a Json  <see cref="DateTime"/> representation to a <see cref="DateTime"/>
		/// </summary>
		/// <returns>A <see cref="DateTime"/></returns>
		/// <param name="value">a <see cref="Int64"/> representing the # of ticks for the  <see cref="DateTime"/></param>
		public override object FromJson(object value) {
			return new DateTime((long)value);
		}

		/// <summary>
		/// Convert a <see cref="DateTime"/> value to a Json representation
		/// </summary>
		/// <returns>a <see cref="Int64"/> representing the # of ticks in the <c>DateTime</c></returns>
		/// <param name="value">a <c>DateTime</c></param>
		public override object ToJson(object value) {
			return ((DateTime)value).Ticks;
		}
	}
}

