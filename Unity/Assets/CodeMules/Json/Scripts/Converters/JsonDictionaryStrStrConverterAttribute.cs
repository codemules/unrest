using System;
using System.Collections.Generic;

namespace CodeMules.Json {
	/// <summary>
	/// Convert a <c>Dictionary{string,object}</c> to
	/// a <c>Dictionary{string,string}</c> with optional case-sensitivity
	/// </summary>
	public class JsonDictionaryStrStrConverterAttribute : JsonConverterAttribute {
	    /// <summary>
	    /// Create an instance of the <see cref="JsonDictionaryStrStrConverterAttribute"/>
	    /// </summary>
	    public JsonDictionaryStrStrConverterAttribute() : this(true) {}

	    /// <summary>
	    /// Create an instance of the <see cref="JsonDictionaryStrStrConverterAttribute"/>
	    /// </summary>
	    /// <param name="caseSensitive">
	    /// Determines whether the key lookup for the dictionary is case-sensitive, or
	    /// case-INsensitive
	    /// </param>
	    public JsonDictionaryStrStrConverterAttribute(bool caseSensitive) {
	        CaseSensitive = caseSensitive;
	    }

	    /// <summary>
		/// Determine if the dictionary keys case sensitive?
		/// </summary>
		/// <value>
		/// <c>true</c> if the keys in the dictionary are case-sensitive, 
		/// <c>false</c> if they are case-INsensitive
		/// </value>
		public bool CaseSensitive { get; set; }

		/// <summary>
		/// Convert a <c>Dictionary{string,object}</c> into
		/// a <c>Dictionary{string,string}</c>
		/// </summary>
		/// <returns>
		/// The <c>Dictionary{string,string}</c> containing the values from <paramref name="value"/> 
		/// </returns>
		/// <param name="value">
		/// The input value which should always be of type <c>IDictionary{string,object}</c>
		/// </param>
		public override object FromJson(object value) {
			IDictionary<string, object> dictionary = value as IDictionary<string, object>;

			// ??
			if (dictionary == null)
				return null;
			
			// create the result
			IDictionary<string, string> result = new Dictionary<string, string>(
				CaseSensitive ? StringComparer.InvariantCulture : StringComparer.InvariantCultureIgnoreCase
			);

			// translate each value in the source to the destination
			foreach (var kvp in dictionary) {
				result[kvp.Key] = Convert.ToString(kvp.Value);
			}

			return result;
		}
	}
}
