﻿using System;

namespace CodeMules.Json {
	/// <summary>
	/// <see cref="TimeSpan"/> conversion to Json representation using ticks (i.e, long)
	/// </summary>
	/// <remarks>
	/// Converts the <see cref="TimeSpan"/> to/from ticks (i.e., long)
	/// </remarks>
	public class JsonTimeSpanTicksConverterAttribute : JsonConverterAttribute {
		/// <summary>
		/// Convert a Json <see cref="Int64"/> ticks value into a <see cref="TimeSpan"/>
		/// </summary>
		/// <returns>The <see cref="TimeSpan"/></returns>
		/// <param name="value">a <see cref="Int64"/> representing the # of ticks for the <see cref="TimeSpan"/></param>
		public override object FromJson(object value) {
			return new TimeSpan((long)value);
		}

		/// <summary>
		/// Convert a <see cref="TimeSpan"/> value to Json representation
		/// </summary>
		/// <returns>a <see cref="Int64"/> representing the # of ticks in the <see cref="TimeSpan"/></returns>
		/// <param name="value">a <c>TimeSpan</c></param>
		public override object ToJson(object value) {
			return ((TimeSpan)value).Ticks;
		}
	}
}

