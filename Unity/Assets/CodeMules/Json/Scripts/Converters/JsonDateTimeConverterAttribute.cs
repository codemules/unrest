﻿using System;

namespace CodeMules.Json {
	/// <summary>
	/// Json <see cref="DateTime"/> converter using a specified or default format
	/// </summary>
	public class JsonDateTimeConverterAttribute : JsonConverterAttribute {
		#region Constants
		/// <summary>
		/// The default date format (ISO)
		/// </summary>
		public const string DefaultDateFormat = "o"; 
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonDateTimeConverterAttribute"/> class.
		/// </summary>
		/// <remarks>
		/// <see cref="DateFormat"/> is initialized to the default format for ISO 8601 date/time representation
		/// </remarks>
		public JsonDateTimeConverterAttribute() {
			DateFormat = DefaultDateFormat;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Json.JsonDateTimeConverterAttribute"/> class.
		/// </summary>
		/// <param name="dateFormat">A custom date format string</param>
		public JsonDateTimeConverterAttribute(string dateFormat) {
			DateFormat = dateFormat;
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// Get/Set the date format string used for json serialization
		/// </summary>
		/// <value>The date format string</value>
		public string DateFormat { get; set; }
		#endregion
		
		#region Conversion Overrides
		public override object FromJson(object value) {
			DateTime result;

			if (!DateTime.TryParse(value.ToString(),out result))
			{
				return DateTime.MinValue;
			}

			return result.ToUniversalTime() ;
		}
		
		public override object ToJson(object value) {
			return ((DateTime)value).ToUniversalTime().ToString(DateFormat ?? DefaultDateFormat);
		}
		#endregion
	}
}
