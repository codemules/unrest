﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CodeMules.Json {
	/// <summary>
	/// Helper methods for pretty formatting Json representations
	/// </summary>
	public static class JsonFormatter {
		/// <summary>
		/// Format Json an encoded json object in a pretty way
		/// </summary>
		/// <returns>A formatted Json string</returns>
		/// <param name="jsonObject">The Json object to be formatted</param>
		/// <param name="indentSize">The # of spaces per indent level</param>
		public static string FormatJson(IDictionary<string, object> jsonObject,int indentSize = 4) {
			return string.Format("{{{1}{0}}}{0}", Environment.NewLine, FormatJson((IDictionary)jsonObject,indentSize));
		}

		/// <summary>
		/// Get a key name (if any) for the json encoding
		/// </summary>
		/// <returns>The key name.</returns>
		/// <param name="key">Key.</param>
		private static string GetKeyName(string key) {
			string result = string.Empty;

			if (!String.IsNullOrEmpty(key)) {
				result = string.Format("\"{0}\": ", key);
			}

			return result;
		}

		/// <summary>
		/// Append an encoded item 
		/// </summary>
		/// <param name="kvp">Keyvalue pair with key and value</param>
		/// <param name="indentLevel">The indentation level</param>
		/// <param name="indent">The ident string</param>
		/// <param name="sb">The string builder to append to</param>
		private static void AppendItem(KeyValuePair<string, object> kvp, int indentLevel, string indent, StringBuilder sb) {
		    IDictionary dict = kvp.Value as IDictionary;
		    IList<object> items = kvp.Value as IList<Object>;

		    if (dict != null) {
		        if (dict.Count > 0)
		        {
		            sb.AppendFormat(
		                "{0}{1}{{{2}{3}{0}}}",
		                indent,
		                GetKeyName(kvp.Key),
		                FormatJson(dict, indentLevel + 4),
		                Environment.NewLine
		                );
		        }
		        else
		        {
		            sb.AppendFormat(
		                "{0}{1}{{ }}",
		                indent,
		                GetKeyName(kvp.Key)
		            );
		        }
		    } else if (items != null) {
				sb.AppendFormat("{0}{1}[", indent, GetKeyName(kvp.Key));

		        if (items.Count > 0)
		        {
		            int itemCount = 0;

		            int arrayIndentLevel = indentLevel + 2;
		            string arrayIndent = new string(' ', arrayIndentLevel);

		            foreach (object obj in items)
		            {
		                if (itemCount++ > 0)
		                {
		                    sb.Append(',');
		                }

		                sb.AppendLine();

		                AppendItem(new KeyValuePair<string, object>(string.Empty, obj), arrayIndentLevel, arrayIndent, sb);
		            }

		            sb.AppendFormat("{1}{0}]", indent, Environment.NewLine);
		        }
		        else
		        {
		            sb.AppendFormat("]");
		        }
		    } else {
				sb.AppendFormat("{0}{1}{2}", indent, GetKeyName(kvp.Key), kvp.Value);
			}
		}

		/// <summary>
		/// Recursive entry-point for formatting a Json encoded data structure to Json string
		/// </summary>
		/// <returns>The json string for the element</returns>
		/// <param name="jsonObject">Json data element</param>
		/// <param name="indentLevel">The indentation level</param>
		private static string FormatJson(IDictionary jsonObject, int indentLevel) {
			StringBuilder sb = new StringBuilder();

			string indent = new string(' ', indentLevel);

			int itemCount = 0;

			foreach (object key in jsonObject.Keys) {
				if (itemCount++ > 0) {
					sb.Append(',');
				}

				sb.AppendLine();

				AppendItem(new KeyValuePair<string, object>(key as string, jsonObject[key]), indentLevel, indent, sb);
			}

			return sb.ToString();
		}		
	}
}

