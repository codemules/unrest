using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.IO;
using System.Collections;
using System.Text;

namespace CodeMules.Json {
	/// <summary>
	/// Helper methods for advanced serialization/deserialization of Json
	/// </summary>
	public static class JsonSerializer {
		#region (Pre)Serialize and Deserialize methods
		/// <summary>
		/// Convert an object into the best form for Json serialization
		/// </summary>
		/// <param name="toBeSerialized">The object to be serialized</param>
		/// <remarks>
		/// If <paramref name="toBeSerialized"/> has the <see cref="JsonSerializationAttribute"/>
		/// then it will be serialized to an <c>IDictionary{string,object}</c> prior to
		/// being serialized into Json, otherwise this method is pass-thru
		/// </remarks>
		public static object JsonPreSerialize(object toBeSerialized) {
			// Nothing to serialize
			if (toBeSerialized == null)
				return null;

			object result = null ;

			if (IsJsonSerializable(toBeSerialized.GetType())) {
				result = AutoSerialize(toBeSerialized);
			}

			// return the original value
			return result ?? toBeSerialized ;
		}

		/// <summary>
		/// Standard Json serialization to string format
		/// </summary>
		/// <param name="toBeSerialized">The object graph to be serialized</param>
		public static string Serialize(object toBeSerialized) {
			return MiniJSON.Json.Serialize(toBeSerialized,JsonPreSerialize);
		}

		/// <summary>
		/// Standard Json deserialization from string format
		/// </summary>
		/// <param name="jsonString">the Json encoded string</param>
		/// <returns>
		/// The <see cref="IDictionary{K,V}"/> representation of <paramref name="jsonString"/>
		/// </returns>
		public static IDictionary<string,object> Deserialize(string jsonString) {
            return MiniJSON.Json.Deserialize(jsonString) as IDictionary<string,object> ;
		}
		#endregion
		
		#region Auto-Deserialize methods
		/// <summary>
		/// Autos the deserialize.
		/// </summary>
		/// <returns>The deserialize.</returns>
		/// <param name="type">Type.</param>
		/// <param name="values">Values.</param>
		public static object AutoDeserialize(Type type, IDictionary<string, object> values) {
			// if it is an interface or abstract, get the type to deserialize as,
			// otherwise it will return the original value of type
			type = GetDeserializeAsType(type,type);
			
			object instance = Activator.CreateInstance(type);

			foreach (PropertyInfo pinfo in type.GetProperties(
					BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
			        ) 
			{
				// Don't do specials (e.g., T this[string] {get ; })
				if (pinfo.IsSpecialName)
					continue;

				// [JsonIgnore]
				if (IsIgnored(pinfo))
					continue;

				// get a key name associated with the property
				string keyName = GetPropertyKeyName(pinfo) ;

				object value = null;

				// find the key and use the value
				if (values.ContainsKey(keyName)) {
					value = values[keyName];
				} else {
					JsonDefaultValueAttribute jsonDefaultValue = GetJsonDefaultValue(pinfo);

					if (jsonDefaultValue != null) {
						value = jsonDefaultValue.GetDefaultValue();
					} else {
						// No value? See if there is a [DefaultValue(obj)]
						object[] attrs = pinfo.GetCustomAttributes(typeof(DefaultValueAttribute), false);

						// use the default value if found
						if ((attrs != null) && (attrs.Length > 0)) {
							value = (attrs[0] as DefaultValueAttribute).Value;
						}
					}
				}

				JsonConverterAttribute converter = GetJsonConverter(pinfo);

				if (converter != null) {
					value = converter.FromJson(value);
				} else {
					value = ConvertValue(value, pinfo);
				}

				// Convert the value to an appropriate type and set the property
				SetValue(
					pinfo,
					instance,
					value
					);
			}	
			
			return instance;
		}

		/// <summary>
		/// Set the property value
		/// </summary>
		/// <param name="propertyInfo">The property being set</param>
		/// <param name="instance">The instance where the property is being set</param>
		/// <param name="value">The property value</param>
		/// <remarks>
		/// Due to accessibility restrictions, if <paramref name="propertyInfo"/> is not 
		/// <see cref="PropertyInfo"/> form the <c>declaring</c> type, then we get the
		/// declaring <see cref="PropertyInfo"/> and use that for the set. This bypasses
		/// any access restrictions imposed for the derived class
		/// </remarks>
		private static void SetValue(PropertyInfo propertyInfo, object instance, object value) {
			if (propertyInfo.DeclaringType != propertyInfo.ReflectedType) {
				propertyInfo = propertyInfo.DeclaringType.GetProperty(
								propertyInfo.Name, 
								BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
								);
			}

			propertyInfo.SetValue(instance, value, null);
		}

		/// <summary>
		/// Auto-deserialize an <see cref="IDictionary{K,V}"/> to a specified type
		/// </summary>
		/// <returns>
		/// An instance of <typeparamref name="T"/> with values from <paramref name="values"/>
		/// </returns>
		/// <param name="values">the serialized property values for the instance being deserialized</param>
		/// <typeparam name="T">The type of the instance being deserialized</typeparam>
        public static T AutoDeserialize<T>(
							IDictionary<string,object> values
							) where T : class, new() 
		{
			return (T) AutoDeserialize(typeof(T),values) ;
        }

		/// <summary>
		/// Auto-deserialize json string to a specified type
		/// </summary>
		/// <returns>
		/// An instance of <typeparamref name="T"/> with values from <paramref name="jsonEncoding"/>
		/// </returns>
		/// <param name="jsonEncoding">the json string representation of the instance being deserialized</param>
		/// <typeparam name="T">The type of the instance being deserialized</typeparam>
		public static T AutoDeserialize<T>(
							string jsonEncoding
							) where T : class, new() 
		{
            return AutoDeserialize<T>(
					MiniJSON.Json.Deserialize(jsonEncoding) as IDictionary<string,object>
				);
        }
		#endregion

		#region Implementation
		/// <summary>
		/// Serialize the properties of an object
		/// </summary>
		/// <returns>The properties as a dictionary</returns>
		/// <param name="toBeSerialized">The object to be serialized</param>
		private static IDictionary<string,object> AutoSerialize(object toBeSerialized) {
			IDictionary<string,object> result = new Dictionary<string, object>();

			// Get the public instance properties
            PropertyInfo[] properties = toBeSerialized.GetType().GetProperties(
					BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
					);

			foreach (PropertyInfo propertyInfo in properties) {
				// Don't do specials (e.g., T this[string] {get ; })
				if (propertyInfo.IsSpecialName)
					continue;

				// Check if the property is JsonIgnore
				if (IsIgnored(propertyInfo)) 
					continue;

				// Get the value of the property
				object value = propertyInfo.GetValue(toBeSerialized,null);

				// We only include property if it is non-null
				if (value != null) {
					// look for a custom converter
					JsonConverterAttribute converter = GetJsonConverter(propertyInfo);

					if (converter != null) {
						value = converter.ToJson(value);
						result.Add(GetPropertyKeyName(propertyInfo), JsonPreSerialize(value));
					} else if (IsEncodeAsString(propertyInfo)) { 
						result.Add(GetPropertyKeyName(propertyInfo), value.ToString());
					} else {
						Func<object, object> encoder;

						if (DefaultEncoders.TryGetValue(value.GetType(), out encoder)) {
							value = encoder(value);
						} else if (value.GetType().IsEnum) {
							// note the presumption of underlying type being Int32
							value = (Int32)value;
						}

						// Use JsonPreSerialize to get the value to add
						result.Add(GetPropertyKeyName(propertyInfo), JsonPreSerialize(value));
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Default serialization encoders
		/// </summary>
		private static IDictionary<Type, Func<object, object>> DefaultEncoders =
			new Dictionary<Type, Func<object, object>>() {
				{ typeof(DateTime), (value) => ((DateTime) value).Ticks },
				{ typeof(TimeSpan), (value) => ((TimeSpan) value).Ticks }
			};


		/// <summary>
		/// Determine if a <see cref="Type"/> specifies the <see cref="JsonSerializationAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="type">The <see cref="Type"/></param>
		public static bool IsJsonSerializable(Type type) {
			object[] attrs = type.GetCustomAttributes(typeof(JsonSerializationAttribute), false);
			return attrs.Length != 0;
		}

		/// <summary>
		/// Determine if a property specifies the <see cref="JsonIgnoreAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="propertyInfo">The property info</param>
		private static bool IsIgnored(PropertyInfo propertyInfo) {
			object [] attrs = propertyInfo.GetCustomAttributes(typeof(JsonIgnoreAttribute), true);
			return attrs.Length != 0;
		}

		/// <summary>
		/// Determine if a property should encoded as a string (via <see cref="object.ToString()"/>)
		/// based on the <see cref="JsonEncodeAsStringAttribute"/> attribute
		/// </summary>
		/// <returns>
		/// <c>true</c> if the attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="propertyInfo">The property info</param>
		private static bool IsEncodeAsString(PropertyInfo propertyInfo) {
			object[] attrs = propertyInfo.GetCustomAttributes(typeof(JsonEncodeAsStringAttribute), true);
			return attrs.Length != 0;
		}

		/// <summary>
		/// Determine if a property specifies the <see cref="JsonConverterAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the <see cref="JsonConverterAttribute"/> attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="propertyInfo">The property info</param>
		private static JsonConverterAttribute GetJsonConverter(PropertyInfo propertyInfo) {
			JsonConverterAttribute [] attrs = propertyInfo.GetCustomAttributes(typeof(JsonConverterAttribute), true)
			                                              as JsonConverterAttribute [] ;
			return ((attrs != null) && (attrs.Length > 0)) ? attrs[0] : null ;
		}

		/// <summary>
		/// Determine if a property specifies the <see cref="JsonDefaultValueAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the <see cref="JsonDefaultValueAttribute"/> attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="propertyInfo">The property info</param>
		private static JsonDefaultValueAttribute GetJsonDefaultValue(PropertyInfo propertyInfo) {
			JsonDefaultValueAttribute[] attrs = propertyInfo.GetCustomAttributes(typeof(JsonDefaultValueAttribute), true)
														  as JsonDefaultValueAttribute[];
			return ((attrs != null) && (attrs.Length > 0)) ? attrs[0] : null;
		}

		/// <summary>
		/// Determine if a <see cref="Type"/> specifies the <see cref="JsonDeserializeAsTypeAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the <see cref="JsonDeserializeAsTypeAttribute"/> attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="type">The type</param>
		private static JsonDeserializeAsTypeAttribute GetJsonDeserializeAs(Type type) {
			JsonDeserializeAsTypeAttribute[] attrs = type.GetCustomAttributes(typeof(JsonDeserializeAsTypeAttribute), true)
														  as JsonDeserializeAsTypeAttribute[];
			return ((attrs != null) && (attrs.Length > 0)) ? attrs[0] : null;
		}

		/// <summary>
		/// Determine if a property specifies the <see cref="JsonDeserializeAsTypeAttribute"/>
		/// </summary>
		/// <returns>
		/// <c>true</c> if the <see cref="JsonDeserializeAsTypeAttribute"/> attribute is present, <c>false</c> otherwise
		/// </returns>
		/// <param name="propertyInfo">The type</param>
		private static JsonDeserializeAsTypeAttribute GetJsonDeserializeAs(PropertyInfo propertyInfo) {
			JsonDeserializeAsTypeAttribute[] attrs = propertyInfo.GetCustomAttributes(typeof(JsonDeserializeAsTypeAttribute), true)
														  as JsonDeserializeAsTypeAttribute[];
			return ((attrs != null) && (attrs.Length > 0)) ? attrs[0] : null;
		}

		/// <summary>
		/// Get the name of the key for the dictionary
		/// </summary>
		/// <returns>The key name</returns>
		/// <param name="propertyInfo">The property info</param>
		/// <remarks>
		/// If the property specifies the <see cref="JsonPropertyAttribute"/> the specified
		/// name is used, otherwise we use <c>PropertyInfo.Name</c>
		/// </remarks>
		private static string GetPropertyKeyName(PropertyInfo propertyInfo) {
			// generally speaking, Json uses lowerCamelCase for entity names
			// so by doing this we avoid the over use of [JsonProperty("xxx")] 
			// as .NET property names are UpperCamelCase
			string result = GetLowerCamelCase(propertyInfo.Name) ;

			object [] attrs = propertyInfo.GetCustomAttributes(typeof(JsonPropertyAttribute), true);

			if (attrs.Length > 0) {
				result = ((JsonPropertyAttribute)attrs[0]).Key;
			}

			return result ;
		}

		/// <summary>
		/// Convert an UpperCamelCase string (assumed) to lowerCamelCase
		/// </summary>
		/// <returns>The lower camel case representation</returns>
		/// <param name="str">the UpperCamelCase string</param>
		/// <remarks>
		/// I really dont like this from a GC pov, but lowerCamelCase
		/// for Json properties is prevalent. Saves a lot of unnecessary stuff like
		/// <c>[JsonProperty("xxx")] string Xxx { get ; set; }</c>
		/// </remarks>
		private static string GetLowerCamelCase(string str) {
			StringBuilder sb = new StringBuilder(str);
			sb[0] = char.ToLower(sb[0]);
			return sb.ToString();
		}

		#endregion

		#region Conversion helpers
		/// <summary>
		/// Determine if a type is eligible to have deserialize-as-type information
		/// </summary>
		/// <returns>
		/// <c>true</c> if the type meets the criteria for being eligible to use
		/// deserialize-as-type information, <c>false</c> otherwise
		/// </returns>
		/// <param name="type">The type to check</param>
		/// <remarks>
		/// Only interfaces and abstract classes or arrays of interfaces/abstract classes
		/// are deserialize-as-type eligible
		/// </remarks>
		private static bool IsDeserializeAsTypeEligible(Type type) {
			if (type.IsInterface || type.IsAbstract)
				return true;
			    
			return IsArrayLike(type) && IsDeserializeAsTypeEligible(type.GetElementType());
		}
		
		/// <summary>
		/// Given a type determine its deserialize-as-type (if applicable)
		/// </summary>
		/// <returns>the type to deserialize as, or <paramref name="defaultType"/></returns>
		/// <param name="type">The target type</param>
		/// <param name="defaultType">
		/// The value to return if <paramref name="type"/> does not specify deserialize-as-type
		/// information
		/// </param>
		private static Type GetDeserializeAsType(Type type,Type defaultType = null) {
			Type result = defaultType;

			if (IsDeserializeAsTypeEligible(type)) {
				JsonDeserializeAsTypeAttribute jdaa = GetJsonDeserializeAs(type);

				if (jdaa != null) {
					result = jdaa.DeserializeAsType;
				}
			}

			return result ;
		}

		/// <summary>
		/// Given a property determine its deserialize-as-type (if applicable)
		/// </summary>
		/// <returns>the type to deserialize as, or <paramref name="defaultType"/></returns>
		/// <param name="propertyInfo">The property</param>
		/// <param name="defaultType">
		/// The value to return if <paramref name="propertyInfo"/> does not specify deserialize-as-type
		/// information
		/// </param>
		private static Type GetDeserializeAsType(PropertyInfo propertyInfo,Type defaultType = null) {
			Type result = defaultType ;

			if (IsDeserializeAsTypeEligible(propertyInfo.PropertyType)) {
				JsonDeserializeAsTypeAttribute jdaa = GetJsonDeserializeAs(propertyInfo);

				if (jdaa != null) {
					result = jdaa.DeserializeAsType;
				}
			}

			return result ;
		}		

		/// <summary>
		/// Convert the json representation of an enum into the actual enum type
		/// </summary>
		/// <returns>The enum value</returns>
		/// <param name="enumType">The type of the enum</param>
		/// <param name="value">a representation of the enum</param>
		private static object ConvertEnum(Type enumType,object value) {
			if (value.GetType().IsEnum || (value.GetType() == Enum.GetUnderlyingType(enumType)))
				return value;

			string strVal = value as string;
			if (strVal != null) {
				return Enum.Parse(enumType, strVal, true);
			}

			if (value is long) {
				return Convert.ChangeType(value, Enum.GetUnderlyingType(enumType));
			}

			// Hmmmm
			return value;
		}

		/// <summary>
		/// Convert values to an array
		/// </summary>
		/// <returns>The array</returns>
		/// <param name="values">The list of values</param>
		/// <param name="elementType">The array element type</param>
		/// <param name="deserializeAs">Optional type to deserialize elements as</param>
		private static Array ConvertArray(IList values,Type elementType, Type deserializeAs = null) {
			Array array = Array.CreateInstance(elementType, values.Count);
			
			for (int i = 0; i < array.Length; i++) {
				array.SetValue(ConvertValue(values[i], deserializeAs ?? elementType),i) ;
			}
			
			return array;
		}
		
		/// <summary>
		/// Default deserialization converters 
		/// </summary>
		private static IDictionary<Type,Func<object,object>> DefaultConverters =
			new Dictionary<Type, Func<object, object>>() 
		{
			{ typeof(string), (value) => Convert.ToString(value) },
			{ typeof(Int32), (value) => Convert.ToInt32(value) },
			{ typeof(Int64), (value) => Convert.ToInt64(value) },
			{ typeof(float), (value) => Convert.ToSingle(value) },
			{ typeof(double), (value) => Convert.ToDouble(value) },
			{ typeof(decimal), (value) => Convert.ToDecimal(value) },
			{ typeof(bool), (value) => Convert.ToBoolean(value) },
			{ typeof(DateTime), (value) => (value is long) ? new DateTime((long)value) : DateTime.Parse(value.ToString()) },
			{ typeof(TimeSpan), (value) => new TimeSpan((long)value) },
			{ typeof(Version), (value) => new Version(value.ToString()) }

		};

		private static bool IsArrayLike(Type expectedType) {
			return expectedType.IsArray ||
				(expectedType == typeof(IEnumerable)) ||
				(expectedType.IsGenericType && (expectedType.GetGenericTypeDefinition() == typeof(IEnumerable<>)));
		}

		/// <summary>
		/// Convert a value to an expected type
		/// </summary>
		/// <returns>The converted value</returns>
		/// <param name="value">The value to convert</param>
		/// <param name="propertyInfo">The type to convert to</param>
		private static object ConvertValue(object value, PropertyInfo propertyInfo) {
			if (IsArrayLike(propertyInfo.PropertyType)) {
				return ConvertValue(value, propertyInfo.PropertyType, GetDeserializeAsType(propertyInfo,null));
			}

			return ConvertValue(value,GetDeserializeAsType(propertyInfo,propertyInfo.PropertyType));
		}

		/// <summary>
		/// Convert a value to an expected type
		/// </summary>
		/// <returns>The converted value</returns>
		/// <param name="value">The value to convert</param>
		/// <param name="expectedType">The type to convert to</param>
		/// <param name="deserializeAsType">The type to convert to for array elements</param>
		private static object ConvertValue(
								object value,
								Type expectedType,
								Type deserializeAsType = null
								) 
		{
            if (value == null)
                return null;

			// if the type is already compatible then no conversion is required
			if (expectedType.IsAssignableFrom(value.GetType()))
				return value;
			    			
			object result = value ;
			IDictionary<string, object> objValue = value as IDictionary<string, object>;

			// If the element type is a "json object", and the expected type is 
			// json serializable we use auto deserialization
			if ((objValue != null) && IsJsonSerializable(expectedType)) {
				result = AutoDeserialize(expectedType, objValue);
			} else {
				//	----------------------------------------------------------------------
				//	Try to find a matching converter. Search the converters specified by
				//	the caller, and/or DefaultConverters if not found
				//	----------------------------------------------------------------------

				Func<object, object> converter = null;

				DefaultConverters.TryGetValue(expectedType, out converter);

				// Perform the conversion if applicable
				if (converter != null) {
					result = converter(value);

					// If we didnt have a specific converter and its an enum
					// do the generic conversion
				} else if (expectedType.IsEnum) {
					result = ConvertEnum(expectedType, value);
				} else if (
					expectedType.IsArray || 
					expectedType == typeof(IEnumerable) ||
					(expectedType.IsGenericType && (expectedType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
				) {
					Type elementType = null;

					if (expectedType == typeof(IEnumerable)) {
						elementType = typeof(object);
					} else if (expectedType.IsGenericType && (expectedType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
					{
						elementType = expectedType.GetGenericArguments()[0];
					} else {
						elementType = expectedType.GetElementType();
					}
					
					result = ConvertArray(
								(IList)value, 
								// the array element type
								elementType,
								// the type to deserialize each element as
								deserializeAsType
								);
				}
			}

            return result;
        }
		#endregion
		
		#region File Helpers
		/// <summary>
		/// Parse/Decode Json from a file
		/// </summary>
		/// <returns>
		/// An <c>IDictionary{string,object}</c> representation of the Json
		/// </returns>
		/// <param name='filePath'>
		/// The path to the file
		/// </param>
		public static IDictionary<string, object> ParseFile(string filePath) {
			StreamReader sr = File.OpenText(filePath);
			try {
				return Deserialize(sr.ReadToEnd());
			} finally {
				sr.Close();
			}
		}

		/// <summary>
		/// Serialize an object graph to Json and write it to a file
		/// </summary>
		/// <param name="toBeSerialized">The object graph to be serialized</param>
		/// <param name="filePath">The path to where the file should be written. If the file already exists, it will be overwritten</param>
		public static void SerializeToFile(object toBeSerialized, string filePath) {
			string json = Serialize(toBeSerialized);

			StreamWriter sw = new StreamWriter(filePath, false);
			try {
				sw.Write(json);
			} finally {
				sw.Close();
			}
		}
		#endregion
	}
}
