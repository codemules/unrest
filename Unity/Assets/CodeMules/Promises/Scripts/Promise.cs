//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of
//	this software and associated documentation files (the "Software"), to deal in
//	the Software without restriction, including without limitation the rights to use,
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//	Software, and to permit persons to whom the Software is furnished to do so,
//	subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

#if USE_OBJECTPOOLING
using CodeMules.ObjectPooling;
#endif


namespace CodeMules.Promises {
	/// <summary>
	/// Default implementation of <see cref="IPromise"/> 
	/// and <see cref="IPromiseController"/>
	/// </summary>
	public class Promise : IPromiseController {
		#region Constants
		/// <summary>
		/// Useful when you actuall want to represent a <c>null</c> result for <see cref="IPromiseController.Result"/>
		/// </summary>
		public static readonly object NullResult = new object();
		
		/// <summary>
		/// Pre-defined exception for use in all cancel situations
		/// </summary>
		/// <remarks>
		/// Possible because the stack-trace is unimportant for cancellations
		/// </remarks>
		protected static readonly OperationCanceledException OperationCanceled = new OperationCanceledException();
		
		/// <summary>
		/// A pre-resolved promise
		/// </summary>
		/// <remarks>
		/// useful in situations where a delivered promise needs to be returned 
		/// </remarks>
		protected static readonly IPromise Resolved = new Promise(true);
		#endregion
		
		#region Fields
		/// <summary>
		/// See <see cref="AlwaysListeners"/>
		/// </summary>
		private List<Action<IPromiseController>> alwaysListeners;
		
		/// <summary>
		/// See <see cref="CanceledListeners"/>
		/// </summary>
		private List<Action> canceledListeners;
		
		/// <summary>
		/// See <see cref="SuccessListeners"/>
		/// </summary>
		private List<Action<object>> successListeners;

		/// <summary>
		/// See <see cref="CatchListeners"/>
		/// </summary>
		private List<Action<Exception>> catchListeners;

		/// <summary>
		/// The delivered promise result value
		/// </summary>
		private object result;
		#endregion

		#region Constructor(s) 
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules..Promises.Promise"/> class.
		/// </summary>
		public Promise() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:CodeMules.Promises.Promise"/> class.
		/// </summary>
		/// <param name="result">the delivered result</param>
		private Promise(object result) {
			this.result = result;
		}
		#endregion

		#region IPromise implementation
		/// <summary>
		/// Add an 'always' handler called when the <c>IPromise</c> is delivered
		/// </summary>
		/// <param name="alwaysHandler">The always handler</param>
		public IPromise Always(Action<IPromiseController> alwaysHandler) {
			// Validation.CheckNotNull(alwaysHandler, "alwaysHandler");

			if (IsDelivered) {
				alwaysHandler(this);
			} else {
				AlwaysListeners.Add(alwaysHandler);
			}

			return this;
		}

		/// <summary>
		/// Add a handler for a canceled <c>IPromise</c>
		/// </summary>
		/// <param name="cancelHandler">the cancellation handler</param>
		public IPromise Canceled(Action cancelHandler) {
			// Validation.CheckNotNull(cancelHandler, "cancelHandler");

			if (IsDelivered) {
				if (IsCanceled) {
					cancelHandler();
				}
			} else {
				CanceledListeners.Add(cancelHandler);
			}

			return this;
		}

		/// <summary>
		/// Add a handler for a failed delivery of the <c>IPromise</c> (including
		/// a canceled <c>IPromise</c>)
		/// </summary>
		/// <param name="exceptionHandler">The exception handler</param>
		public IPromise Catch(Action<Exception> exceptionHandler) {
			// Validation.CheckNotNull(exceptionHandler, "exceptionHandler");

			if (IsDelivered) {
				if (IsFailed) {
					exceptionHandler(Exception);
				}
			} else {
				CatchListeners.Add(exceptionHandler);
			}

			return this;
		}

		/// <summary>
		/// Add a handler for successful delivery of the <c>IPromise</c>
		/// </summary>
		/// <param name="successHandler">The handler</param>
		public IPromise Success(Action<object> successHandler) {
			// Validation.CheckNotNull(successHandler, "successHandler");

			if (IsDelivered) {
				if (IsSuccessful) {
					successHandler(SafeResult);
				}
			} else {
				SuccessListeners.Add(successHandler);
			}

			return this;
		}

		#region Then methods
		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promiseFactory">A function to provide the promise to chain</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		public IPromise Then(Func<IPromise> promiseFactory) {
			Promise resultPromise = new Promise();

			Always(
				_ => {
					if (IsSuccessful) {
						promiseFactory().Success(
							(obj) => resultPromise.Deliver(obj)
						).Catch(
							(ex) => resultPromise.Fail(ex)
						);
					} else {
						resultPromise.Deliver(Exception);
					}
				});
			
			return resultPromise;
		}

		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promise">The promise to chain</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		public IPromise Then(IPromise promise) {
			return Then(() => promise);
		}

		/// <summary>
		/// Chain an <c>IPromise{TResult}</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promiseFactory">A function that provides the promise to chain</param>
		/// <returns>
		/// An <c>IPromise{TResult}</c> representing the link between the two original promises
		/// </returns>
		public IPromise<TResult> Then<TResult>(Func<IPromise<TResult>> promiseFactory) 
			where TResult : class 
		{
			Promise<TResult> resultPromise = new Promise<TResult>();

			Always(
				_ => {
					if (IsSuccessful) {
						promiseFactory().Success(
							(obj) => resultPromise.Deliver(obj)
						).Catch(
							(ex) => resultPromise.Fail(ex)
						);
					} else {
						resultPromise.Deliver(Exception);
					}
				});

			return resultPromise;
		}

		/// <summary>
		/// Chain an <c>IPromise{TResult}</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promise">The promise to chain</param>
		/// <returns>
		/// An <c>IPromise{TResult}</c> representing the link between the two original promises
		/// </returns>
		public IPromise Then<TResult>(IPromise<TResult> promise) where TResult : class {
			return Then(() => promise);
		}
		#endregion

		#region ThenAll methods
		/// <summary>
		/// Create a promise that waits for a collection of promises to be delivered
		/// </summary>
		/// <param name="promises">The promises to wait for</param>
		public static IPromise All(IEnumerable<IPromise> promises) {
			IPromise [] promiseArray = promises.ToArray();

			int count = promiseArray.Length;

			if (count == 0) {
				return Resolved;
			}

			Promise resultPromise = new Promise() ;

			foreach (IPromiseController promise in promiseArray) {
				promise.Always(
					(activePromise) => {
						if (activePromise.IsFailed) {
							resultPromise.Fail(activePromise.Exception);
						} else {
							if (--count == 0) {
								resultPromise.Deliver(true);
							}
						}
					});

				// if we find out early it failed, skip the rest
				if (resultPromise.IsDelivered)
					break;
			}

			return resultPromise;
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises create by <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAll(Func<IEnumerable<IPromise>> promisesFactory) {
			return Then(() => All(promisesFactory()));
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAll(IEnumerable<IPromise> promises) {
			return Then(() => All(promises));
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAll(params IPromise[] promises) {
			return Then(() => All(promises));
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises create by <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAll<TResult>(
			Func<IEnumerable<IPromise<TResult>>> promisesFactory
			) where TResult : class 
		{
			return Then(() => All(promisesFactory().Cast<IPromise>()));
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAll<TResult>(IEnumerable<IPromise<TResult>> promises) 
			where TResult : class 
		{
			return Then(() => All(promises.Cast<IPromise>())) ;
		}
		#endregion

		#region ThenAny methods
		/// <summary>
		/// Create a new promise that waits for any one of a collection of promises to deliver
		/// </summary>
		/// <param name="promises">The collection of promises to wait for</param>
		public static IPromise Any(IEnumerable<IPromise> promises) {
			if (!promises.Any()) {
				return Resolved;
			}

			Promise resultPromise = new Promise();

			foreach (IPromiseController promise in promises) {
			    IPromiseController currentPromise = promise;
					promise.Always(
						(result) => {
							if (currentPromise.IsFailed) {
								resultPromise.Fail(currentPromise.Exception);
							} else {
								resultPromise.Deliver();
							}
						});

				// if we find out early it completed, skip the rest
				if (resultPromise.IsDelivered)
					break;
			}

			return resultPromise;
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAny(Func<IEnumerable<IPromise>> promisesFactory) {
			return Then(() => Any(promisesFactory())) ;
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAny(IEnumerable<IPromise> promises) {
			return Then(() => Any(promises));
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAny(params IPromise[] promises) {
			return Then(() => Any(promises));
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for which
		/// will be <seealso cref="IPromise{TResult}"/>
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAny<TResult>(
			Func<IEnumerable<IPromise<TResult>>> promisesFactory
			) where TResult : class 
		{
			return Then(() => Any(promisesFactory().Cast<IPromise>()));
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises (of type <seealso cref="IPromise{TResult}"/>) to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		public IPromise ThenAny<TResult>(IEnumerable<IPromise<TResult>> promises)
			where TResult : class 
		{
			return Then(() => Any(promises.Cast<IPromise>()));
		}
		#endregion

		#endregion	IPromise implementation

		#region IPromiseController implementation
		/// <summary>
		/// Get the result of a delivered promise
		/// </summary>
		/// <value>
		/// The result of the promise if the promise is delivered and has a
		/// non-exception result
		/// </value>
		/// <exception name="Exception">The exception from <see cref="Exception"/> when non-null</exception>
		/// <exception name="InvalidOperationException">Thrown when the promise is still pending"</exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown if an attempt is made to deliver the promise after it has already been delivered
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown if an attempt is made to deliver a value of <c>null</c>. Deliver <see cref="NullResult"/>
		/// instead
		/// </exception>
		/// <remarks>
		/// The default value for deliver promises is <c>true</c>. 
		/// <para>
		/// Although the value <c>null</c> cannot be delivered directly, it may be indirectly specified by
		/// delivery of <see cref="NullResult"/>. This allows <c>Result == null</c>
		/// </para>
		/// </remarks>
		public object Result {
			get {
				if (result == null) {
					throw new InvalidOperationException("Promise result is not available when the promise is pending");
				}
				
				// If the result represents an exception, re-throw it
				
				Exception ex = Exception;

				if (ex != null) {
					throw ex;
				}

				// return null if result == NullResult (because null cant be assigned directly)
				return SafeResult;
			}
			
			protected set {
				if (value == null) {
					throw new InvalidOperationException("Result value cannot be null. Use NullResult instead");
				}

				if (result != null) {
					throw new InvalidOperationException("Promise already delivered");
				}

				result = value;

				try {
					// Send all notifications/events
					NotifyWhen();
				} finally {
					// release everything related to listeners as they will *never* be used again
					ReleaseListeners();
				}
			}
		}

		/// <summary>
		/// Get the result, as is, never throwing an exception
		/// </summary>
		/// <value>the result value</value>
		/// <remarks>
		/// This value will be <c>null</c> if the promise has not been delivered, and
		/// may also be <c>null</c> when the promise was delivered with <see cref="Promise.NullResult"/>
		/// </remarks>
		public object SafeResult {
			get {
				// return null if result == NullResult (because null cant be assigned directly)
				return ReferenceEquals(result, NullResult) ? null : result;
			}
		}

		/// <summary>
		/// The <see cref="Exception"/> associated with the promise
		/// </summary>
		/// <value>
		/// The exception associated with a failed promise or <c>null</c> if there
		/// is no error
		/// </value>
		/// <exception name="InvalidOperattionException">Thrown when the promise is still pending"</exception>
		public Exception Exception {
			get {
				return result as Exception;
			}
		}

		/// <summary>
		/// Deliver the promise with a result value of <c>true</c>
		/// </summary>
		public IPromiseController Deliver() {
			return Deliver(true);
		}

		/// <summary>
		/// Deliver the result of the promise
		/// </summary>
		/// <param name="result">
		/// The result of the promise
		/// </param>
		public IPromiseController Deliver(object result) {
			Result = result ?? NullResult ;
			return this;
		}

		/// <summary>
		/// Fail the promise
		/// </summary>
		/// <param name="exception">The exception that occurred</param>
		public IPromiseController Deliver(Exception exception) {
			Result = exception;
			return this;
		}

		/// <summary>
		/// Deliver the result of this promise as the result of another promise
		/// </summary>
		/// <param name="promise">The promise that has the result being delivered</param>
		/// <remarks>
		/// This is equivalent to:
		/// <c>
		/// Deliver(promise.SafeResult ?? NullResult)
		/// </c></remarks>
		public IPromiseController Deliver(IPromiseController promise) {
			Result = promise.SafeResult ?? NullResult;
			return this;
		}

		/// <summary>
		/// Break the Promise
		/// </summary>
		/// <remarks>
		/// Alias for <see cref="Deliver(System.Exception)"/> where <c>exception</c> is
		/// an instance of <see cref="OperationCanceledException"/>
		/// </remarks>
		public IPromiseController Cancel() {
			result = OperationCanceled;
			return this;
		}

		/// <summary>
		/// Alias for <see cref="Deliver(System.Exception)"/>
		/// </summary>
		/// <param name="exception">The exception associated with the failure</param>
		public IPromiseController Fail(Exception exception) {
			return Deliver(exception);
		}

		/// <summary>
		/// Determine if the promise was delivered successfully
		/// </summary>
		/// <value>The is successful.</value>
		public bool IsSuccessful {
			get {
				return IsDelivered && !IsFailed;
			}
		}

		/// <summary>
		/// Determine if the promise is still pending delivery
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise is not delivered, <c>false</c> if the
		/// promise has been delivered
		/// </value>
		/// <seealso cref="IsDelivered"/>
		public bool IsPending {
			get {
				return !IsDelivered;
			}
		}

		/// <summary>
		/// Determine if the promise is canceled
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise is canceled, <c>false</c> otherwise
		/// </value>
		public bool IsCanceled {
			get {
				return (Exception != null) && (Exception is OperationCanceledException);
			}
		}

		/// <summary>
		/// Determine if the promise is delivered with a failure
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise was delivered with an <see cref="System.Exception"/>, 
		/// <c>false</c> if the promise is not delivered, or has a non-exception result
		/// </value>
		public bool IsFailed {
			get {
				return (Exception != null);
			}
		}

		/// <summary>
		/// Determine if the promise has been delivered
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise has been delivered, <c>false</c> if the
		/// result is still pending
		/// </value>
		/// <seealso cref="IsPending"/>
		public bool IsDelivered {
			get {
				return result != null;
			}
		}
		#endregion

		#region Implementation
		/// <summary>
		/// Get the list of 'always' listeners
		/// </summary>
		/// <value>The list of always listeners</value>
		private List<Action<IPromiseController>> AlwaysListeners {
			get {
				if (alwaysListeners == null) {
                    #if USE_OBJECTPOOLING
					alwaysListeners = ActionObjectListPool.Acquire();
                    #else
				    alwaysListeners = new List<Action<IPromiseController>>();
                    #endif
				}

				return alwaysListeners;
			}
		}

		/// <summary>
		/// Get the list of 'success' listeners
		/// </summary>
		/// <value>The list of success listeners</value>
		private List<Action<object>> SuccessListeners {
			get {
				if (successListeners == null) {
                    #if USE_OBJECTPOOLING
					successListeners = ActionObjectListPool.Acquire();
                    #else
				    successListeners = new List<Action<object>>();
				    #endif
				}

				return successListeners;
			}
		}

		/// <summary>
		/// Get the list of 'cancel' listeners
		/// </summary>
		/// <value>The list of cancel listeners</value>
		private List<Action> CanceledListeners {
			get {
				if (canceledListeners == null) {
                    #if USE_OBJECTPOOLING
					canceledListeners = ActionListPool.Acquire();
                    #else
				    canceledListeners = new List<Action>();
				    #endif
				}

				return canceledListeners;
			}
		}

		/// <summary>
		/// Get the list of 'catch' listeners
		/// </summary>
		/// <value>The list of catch listeners</value>
		protected List<Action<Exception>> CatchListeners {
			get {
				if (catchListeners == null) {
                    #if USE_OBJECTPOOLING
					catchListeners = ActionExceptionListPool.Acquire();
                    #else
				    catchListeners = new List<Action<Exception>>();
				    #endif
				}

				return catchListeners;
			}
		}

		/// <summary>
		/// Notify the listeners based on the delivery status
		/// </summary>
		protected virtual void NotifyWhen() {
			if (IsSuccessful) {
				if (successListeners != null) {
					SendNotifications(successListeners, SafeResult);
				}
			} else {
				if (catchListeners != null) {
					SendNotifications(catchListeners, Exception);
				}

				if (IsCanceled && (canceledListeners != null)) {
					SendNotifications(canceledListeners);
				}
			}

			if (alwaysListeners != null) {
				SendNotifications(alwaysListeners,this);
			}
		}

		/// <summary>
		/// Helper method to send notifications to listeners
		/// </summary>
		/// <param name="actions">The list of actions to invoke</param>
		/// <remarks>
		/// Exceptions thrown by listeners are trapped/ignored. All listeners
		/// will receive the notification
		/// </remarks>
		private void SendNotifications(IList<Action> actions) {
			for (int i = 0; i < actions.Count; i++) {
				try {
					actions[i]();
				} catch(Exception) {
					// TODO: Log something here . . . 
				}
			}
		}

		/// <summary>
		/// Helper method to send notifications to listeners
		/// </summary>
		/// <param name="actions">The list of actions to invoke</param>
		/// <param name="obj">The parameter to send</param>
		/// <remarks>
		/// Exceptions thrown by listeners are trapped/ignored. All listeners
		/// will receive the notification
		/// </remarks>
		private void SendNotifications(IList<Action<object>> actions,object obj) {
			for (int i = 0; i < actions.Count; i++) {
				try {
					actions[i](obj);
				} catch (Exception) {
					// TODO: Log something here . . . 
				}
			}
		}

	    /// <summary>
	    /// Helper method to send notifications to listeners
	    /// </summary>
	    /// <param name="actions">The list of actions to invoke</param>
	    /// <param name="obj">The parameter to send</param>
	    /// <remarks>
	    /// Exceptions thrown by listeners are trapped/ignored. All listeners
	    /// will receive the notification
	    /// </remarks>
	    private void SendNotifications(IList<Action<IPromiseController>> actions,IPromiseController promise) {
	        for (int i = 0; i < actions.Count; i++) {
	            try {
	                actions[i](promise);
	            } catch (Exception) {
	                // TODO: Log something here . . .
	            }
	        }
	    }


	    /// <summary>
		/// Helper method to send notifications to listeners
		/// </summary>
		/// <param name="actions">The list of actions to invoke</param>
		/// <param name="exception">The exception to send</param>
		/// <remarks>
		/// Exceptions thrown by listeners are trapped/ignored. All listeners
		/// will receive the notification
		/// </remarks>
		private void SendNotifications(IList<Action<Exception>> actions,Exception exception) {
			for (int i = 0; i < actions.Count; i++) {
				try {
					actions[i](exception);
				} catch (Exception) {
					// TODO: Log something here . . . 
				}
			}
		}

		/// <summary>
		/// Release the lists of notification listeners
		/// </summary>
		protected virtual void ReleaseListeners() {
			if (alwaysListeners != null) {
                #if USE_OBJECTPOOLING
				ActionObjectListPool.ReleaseSafe(ref alwaysListeners);
                #else
			    alwaysListeners.Clear();
			    alwaysListeners = null;
                #endif
			}

			if (canceledListeners != null) {
                #if USE_OBJECTPOOLING
				ActionListPool.ReleaseSafe(ref canceledListeners);
                #else
			    canceledListeners.Clear();
			    canceledListeners = null;
                #endif
			}

			if (successListeners != null) {
                #if USE_OBJECTPOOLING
				ActionObjectListPool.ReleaseSafe(ref successListeners);
                #else
			    successListeners.Clear();
			    successListeners = null;
                #endif
			}

			if (catchListeners != null) {
                #if USE_OBJECTPOOLING
				ActionExceptionListPool.ReleaseSafe(ref catchListeners);
                #else
			    catchListeners.Clear();
			    catchListeners = null;
                #endif
			}
		}

		/// <summary>
		/// Clear the fields of the promise
		/// </summary>
		/// <remarks>
		/// This exists with the intention of being able to re-use the
		/// promise (pooling?) when it a pattern of re-use becomes clear
		/// </remarks>
		protected virtual void Clear() {
			ReleaseListeners();
			result = null;
		}
		#endregion

        #if USE_OBJECTPOOLING

		#region List<Action> Pool Implementation
		private static IObjectPool<List<Action>> actionListPool;
		
		private static IObjectPool<List<Action>> ActionListPool {
			get {
				if (actionListPool == null) {
					actionListPool = new ObjectPool<List<Action>>(
						"List<Action>",
						() => new List<Action>(),
						(list) => {
							list.Clear();
							return (list.Capacity <= 4) ? list : null;
						}
					);
				}

				return actionListPool;
			}
		}
		#endregion

		#region List<Action> Pool Implementation
		private static IObjectPool<List<Action<object>>> actionObjectListPool;

		private static IObjectPool<List<Action<object>>> ActionObjectListPool {
			get {
				if (actionObjectListPool == null) {
					actionObjectListPool = new ObjectPool<List<Action<object>>>(
						"List<Action<object>>",
						() => new List<Action<object>>(),
						(list) => {
							list.Clear();
							return (list.Capacity <= 4) ? list : null;
						}
					);
				}

				return actionObjectListPool;
			}
		}
		#endregion

		#region List<Action<Exception>> Pool Implementation
		private static IObjectPool<List<Action<Exception>>> actionExceptionListPool;

		private static IObjectPool<List<Action<Exception>>> ActionExceptionListPool {
			get {
				if (actionExceptionListPool == null) {
					actionExceptionListPool = new ObjectPool<List<Action<Exception>>>(
						"List<Action<Exception>>", 
						() => new List<Action<Exception>>(), 
						(list) => {
							list.Clear();
							return (list.Capacity <= 4) ? list : null;
						});
				}

				return actionExceptionListPool;
			}
		}
		#endregion
        #endif

		#region IDisposable Support
		private bool disposedValue ; // To detect redundant calls

		/// <summary>
		/// Dispose managed/unmanaged resources
		/// </summary>
		/// <param name="disposing"><c>true</c> if this is a direct dispose</param>
		protected virtual void Dispose(bool disposing) {
			if (!disposedValue) {
				if (disposing) {
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~Promise() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		/// <summary>
		/// Releases all resource used by the <see cref="T:CodeMules.Promises.Promise"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose()"/> when you are finished using the <see cref="T:CodeMules.Promises.Promise"/>. The
		/// <see cref="Dispose()"/> method leaves the <see cref="T:CodeMules.Promises.Promise"/> in an unusable state. After calling
		/// <see cref="Dispose()"/>, you must release all references to the <see cref="T:CodeMules.Promises.Promise"/> so the garbage
		/// collector can reclaim the memory that the <see cref="T:CodeMules.Promises.Promise"/> was occupying.</remarks>
		public void Dispose() {
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
