﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of
//	this software and associated documentation files (the "Software"), to deal in
//	the Software without restriction, including without limitation the rights to use,
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//	Software, and to permit persons to whom the Software is furnished to do so,
//	subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

#if USE_OBJECTPOOLING
using CodeMules.ObjectPooling;
#endif

namespace CodeMules.Promises {
	/// <summary>
	/// Default implementation of <see cref="IPromise{T}"/> and 
	/// <see cref="IPromiseController{T}"/>
	/// </summary>
	public class Promise<T> : Promise, IPromiseController<T> where T : class {
		#region Fields
		/// <summary>
		/// See <see cref="AlwaysListeners"/>
		/// </summary>
		private List<Action<IPromiseController<T>>> alwaysListeners;
		
		/// <summary>
		/// See <see cref="SuccessListeners"/>
		/// </summary>
		private List<Action<T>> successListeners ;
		#endregion
		
		#region Implicit Conversion to T
		/// <summary>
		/// Implicitly convert a <c>Promise{T}</c> to type T
		/// </summary>
		/// <returns>The promise result value</returns>
		/// <param name="promise">The promise</param>
		/// <exception name="InvalidOperationException">
		/// Thrown when the promise is still pending as the result is not available
		/// </exception>
		public static implicit operator T(Promise<T> promise) {
			return promise.Result;
		}
		#endregion

		#region IPromiseController<T> implementation
		/// <summary>
		/// Get/Set the promise result
		/// </summary>
		/// <value>The result</value>
		/// <remarks>
		/// Setting a value of <c>null</c> is prohibited. It is possible to use
		/// the alternate property, <see cref="IPromiseController.Result"/>, with <see cref="Promise.NullResult"/>
		/// to achieve a null result value.
		/// </remarks>
		public new T Result {
			get {
				return (T) base.Result;
			}
			
			private set {
				base.Result = value ;
			}
		}

		/// <summary>
		/// Deliver a result of type T
		/// </summary>
		/// <param name="result">the result</param>
		/// <seealso cref="Result"/>
		/// <remarks>
		/// Setting a value of <c>null</c> is prohibited. It is possible to use
		/// the alternate property, <see cref="IPromiseController.Result"/>, with <see cref="Promise.NullResult"/>
		/// to achieve a null result value.
		/// </remarks>
		public IPromiseController<T> Deliver(T result) {
			if (result == null) {
				base.Result = NullResult;
			} else {
				Result = result;
			}

			return this;
		}

		/// <summary>
		/// Deliver a result of type T
		/// </summary>
		/// <param name="result">the result</param>
		/// <seealso cref="Result"/>
		/// <remarks>
		/// Setting a value of <c>null</c> is prohibited. It is possible to use
		/// the alternate property, <see cref="IPromiseController.Result"/>, with <see cref="Promise.NullResult"/>
		/// to achieve a null result value.
		/// </remarks>
		public IPromiseController<T> DeliverSafe(T result) {
			return Deliver(result);
		}

		/// <summary>
		/// Deliver the result of this promise as the result of another promise
		/// </summary>
		/// <param name="promise">The promise that has the result being delivered</param>
		/// <remarks>
		/// This is equivalent to:
		/// <c>
		/// Deliver(promise.SafeResult)
		/// </c></remarks>
		public IPromiseController<T> DeliverSafe(IPromiseController<T> promise) {
			Deliver(promise);
			return this;
		}

		/// <summary>
		/// Fail the promise
		/// </summary>
		/// <param name="exception">The exception that occurred</param>
		/// <returns>
		/// <c>this</c>
		/// </returns>
		public new IPromiseController<T> Deliver(Exception exception) {
			base.Deliver(exception);
			return this;
		}

		/// <summary>
		/// Alias for <see cref="M:CodeMules.Promises.IPromiseController`1.Deliver(System.Exception)" /></summary>
		/// <param name="exception">The exception associated with the failure</param>
		/// <returns>
		/// <c>this</c>
		/// </returns>
		public new IPromiseController<T> Fail(Exception exception) {
			base.Fail(exception);
			return this;
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Release the base class listeners and our listeners
		/// </summary>
		protected override void ReleaseListeners() {
			base.ReleaseListeners();

			if (alwaysListeners != null) {
			    #if USE_OBJECTPOOLING
				ActionPromiseListPool.ReleaseSafe(ref alwaysListeners);
                #else
			    alwaysListeners.Clear();
			    alwaysListeners = null;
                #endif
			}

			if (successListeners != null) {
                #if USE_OBJECTPOOLING
				ActionListPool.ReleaseSafe(ref successListeners);
                #else
			    successListeners.Clear();
			    successListeners = null;
                #endif
			}
		}
		
		/// <summary>
		/// Notify our listeners, then the base class listeners
		/// </summary>
		/// <returns>The when.</returns>
		protected override void NotifyWhen() {
			if (IsSuccessful) {
				if (successListeners != null) {
					SendNotifications(successListeners, Result);
				}
			}

			if (alwaysListeners != null) {
				SendNotifications(alwaysListeners, this) ;
			}

			base.NotifyWhen();
		}
		#endregion

		#region IPromise<T> implementation
		/// <summary>
		/// Add an 'always' handler called when the <c>IPromise{T}</c> is delivered
		/// </summary>
		/// <param name="alwaysHandler">The always handler</param>
		public IPromise<T> Always(Action<IPromiseController<T>> alwaysHandler) {
			if (!IsDelivered) {
				AlwaysListeners.Add(alwaysHandler);
			} else {
				alwaysHandler(this);
			}

			return this;
		}

		/// <summary>
		/// Add a handler for successful delivery of the <c>IPromise{T}</c>
		/// </summary>
		/// <param name="successHandler">The handler</param>
		public IPromise<T> Success(Action<T> successHandler) {
			if (!IsDelivered) {
				SuccessListeners.Add(successHandler);
			} else {
				if (IsSuccessful) {
					successHandler(Result);
				}
			}

			return this;
		}

		/// <summary>
		/// Add a handler for a failed delivery of the <c>IPromise{T}</c> (including
		/// a canceled <c>IPromise{T}</c>)
		/// </summary>
		/// <param name="catchHandler">The exception handler</param>
		public new IPromise<T> Catch(Action<Exception> catchHandler) {
			return base.Catch(catchHandler) as IPromise<T> ;
		}

		/// <summary>
		/// Add a handler for a canceled <c>IPromise{T}</c>
		/// </summary>
		/// <param name="cancelHandler">the cancellation handler</param>
		public new IPromise<T> Canceled(Action cancelHandler) {
			return base.Canceled(cancelHandler) as IPromise<T>;
		}

		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promiseFactory">A function that provides the promise to chain based 
		/// on the result of this promise</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		public IPromise Then(Func<T, IPromise> promiseFactory) {
			IPromiseController resultPromise = new Promise();
			
			Success(
				(obj) => Then(promiseFactory(obj)).Always(
					(obj2) => resultPromise.Deliver(obj2)
				)
			).Catch(
				(ex) => resultPromise.Deliver(ex)
			);

			return resultPromise;
		}

		/// <summary>
		/// Chain an <c>IPromise{TOther}</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="converter">
		/// A function converts the result of this promise to <typeparamref name="TOther"/> 
		/// </param>
		/// <returns>
		/// An <c>IPromise{TOther}</c> representing the link between the two original promises
		/// </returns>
		public IPromise<TOther> Then<TOther>(Func<T, TOther> converter) where TOther : class {
			Promise<TOther> resultPromise = new Promise<TOther>();

			Success(
				(result) => {
					try {
						resultPromise.Deliver(converter(result));
					} catch (Exception ex) {
						resultPromise.Deliver(ex);
					}
				}
			).Catch(
				(ex) => resultPromise.Deliver(ex)
			);
			
			return resultPromise;
		}

		/// <summary>
		/// Chain an <c>IPromise{TOther}</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="converter">
		/// A function converts the result of this promise to <typeparamref name="TOther"/> 
		/// </param>
		/// <returns>
		/// An <c>IPromise{TOther}</c> representing the link between the two original promises
		/// </returns>
		public IPromise<TOther> Then<TOther>(Func<T, IPromise<TOther>> converter) where TOther : class {
			Promise<TOther> resultPromise = new Promise<TOther>();

			Success(
				(result) => {
					try {
						resultPromise.Deliver(converter(result));
					} catch (Exception ex) {
						resultPromise.Deliver(ex);
					}
				}
			).Catch(
				(ex) => resultPromise.Deliver(ex)
			);

			return resultPromise;
		}

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to chain based 
		/// on the result of this promise
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAll(Func<T, IEnumerable<IPromise>> promisesFactory) {
			return Then((result) => All(promisesFactory(result))) ;
		}

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to chain based 
		/// on the result of this promise
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		public IPromise ThenAny(Func<T, IEnumerable<IPromise>> promisesFactory) {
			return Then((result) => Any(promisesFactory(result)));
		}
		#endregion

		#region Implementation
		/// <summary>
		/// The list of 'always' listeners
		/// </summary>
		/// <value>The list always listeners</value>
		private List<Action<IPromiseController<T>>> AlwaysListeners {
			get {
				if (alwaysListeners == null) {
                    #if USE_OBJECTPOOLING
					alwaysListeners = ActionPromiseListPool.Acquire();
                    #else
				    alwaysListeners = new List<Action<IPromiseController<T>>>();
				    #endif
				}

				return alwaysListeners;
			}
		}

		/// <summary>
		/// The list of 'success' listeners
		/// </summary>
		/// <value>The list success listeners</value>
		private List<Action<T>> SuccessListeners {
			get {
				if (successListeners == null) {
				    #if USE_OBJECTPOOLING
					successListeners = ActionListPool.Acquire();
                    #else
				    successListeners = new List<Action<T>>();
				    #endif
				}

				return successListeners;
			}
		}


		/// <summary>
		/// Helper method to send notifications to listeners
		/// </summary>
		/// <param name="actions">The list of actions to invoke</param>
		/// <param name="result">The notification parameter of type T</param>
		/// <remarks>
		/// Exceptions thrown by listeners are trapped/ignored. All listeners
		/// will receive the notification
		/// </remarks>
		private void SendNotifications(IList<Action<T>> actions,T result) {
			for (int i = 0; i < actions.Count; i++) {
				try {
					actions[i](result);
				} catch(Exception) {
					// TODO: Log something here . . . 
				}
			}
		}

		/// <summary>
		/// Helper method to send notifications to listeners
		/// </summary>
		/// <param name="actions">The list of actions to invoke</param>
		/// <param name="promise">The promise notification parameter</param>
		/// <remarks>
		/// Exceptions thrown by listeners are trapped/ignored. All listeners
		/// will receive the notification
		/// </remarks>
		private void SendNotifications(IList<Action<IPromiseController<T>>> actions, IPromiseController<T> promise) {
			for (int i = 0; i < actions.Count; i++) {
				try {
					actions[i](promise);
				} catch(Exception) {
					// TODO: Log something here . . . 
				}
			}
		}
		#endregion

        #if USE_OBJECTPOOLING
		#region List<Action<T>> Pool Implementation
		private static IObjectPool<List<Action<T>>> actionListPool;

		private static IObjectPool<List<Action<T>>> ActionListPool {
			get {
				if (actionListPool == null) {
					actionListPool = new ObjectPool<List<Action<T>>>(
						"List<Action<T>>", 
						() => new List<Action<T>>(),
						(list) => {
							list.Clear();
							return (list.Capacity <= 4) ? list : null;
						});
				}

				return actionListPool;
			}
		}
		#endregion

		#region List<Action<IPromiseController<T>>> Pool Implementation
		private static IObjectPool<List<Action<IPromiseController<T>>>> actionPromiseControllerListPool;

		private static IObjectPool<List<Action<IPromiseController<T>>>> ActionPromiseListPool {
			get {
				if (actionPromiseControllerListPool == null) {
					actionPromiseControllerListPool = new ObjectPool<List<Action<IPromiseController<T>>>>(
						"List<Action<IPromiseController<T>>", 
						() => new List<Action<IPromiseController<T>>>(), 
						(list) => {
							list.Clear();
							return (list.Capacity <= 4) ? list : null;
						});
				}

				return actionPromiseControllerListPool;
			}
		}
		#endregion
        #endif
	}
}

