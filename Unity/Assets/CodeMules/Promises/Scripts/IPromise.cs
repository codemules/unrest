﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of
//	this software and associated documentation files (the "Software"), to deal in
//	the Software without restriction, including without limitation the rights to use,
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//	Software, and to permit persons to whom the Software is furnished to do so,
//	subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic ;

namespace CodeMules.Promises {
	/// <summary>
	/// A promise representing a future result
	/// </summary>
	public interface IPromise : IDisposable {
		/// <summary>
		/// Add a handler for successful delivery of the <c>IPromise</c>
		/// </summary>
		/// <param name="successHandler">The handler</param>
		IPromise Success(Action<object> successHandler);

		/// <summary>
		/// Add a handler for a failed delivery of the <c>IPromise</c> (including
		/// a canceled <c>IPromise</c>)
		/// </summary>
		/// <param name="exceptionHandler">The exception handler</param>
		IPromise Catch(Action<Exception> exceptionHandler);

		/// <summary>
		/// Add a handler for a canceled <c>IPromise</c>
		/// </summary>
		/// <param name="cancelHandler">the cancellation handler</param>
		IPromise Canceled(Action cancelHandler);

		/// <summary>
		/// Add an 'always' handler called when the <c>IPromise</c> is delivered
		/// </summary>
		/// <param name="alwaysHandler">The always handler</param>
		IPromise Always(Action<IPromiseController> alwaysHandler);

		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promise">The promise to chain</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		IPromise Then(IPromise promise);

		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promiseFactory">A function to provide the promise to chain</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		IPromise Then(Func<IPromise> promiseFactory);

		/// <summary>
		/// Chain an <c>IPromise{TResult}</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promise">The promise to chain</param>
		/// <returns>
		/// An <c>IPromise{TResult}</c> representing the link between the two original promises
		/// </returns>
		IPromise Then<TResult>(IPromise<TResult> promise) where TResult : class ;

		/// <summary>
		/// Chain an <c>IPromise{TResult}</c> to the successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promiseFactory">A function that provides the promise to chain</param>
		/// <returns>
		/// An <c>IPromise{TResult}</c> representing the link between the two original promises
		/// </returns>
		IPromise<TResult> Then<TResult>(Func<IPromise<TResult>> promiseFactory) where TResult : class;

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for after the successful completion of
		/// this promise
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAll(params IPromise[] promises);
		
		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAll(IEnumerable<IPromise> promises);

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises create by <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAll(Func<IEnumerable<IPromise>> promisesFactory);

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAll<TResult>(IEnumerable<IPromise<TResult>> promises) where TResult : class;

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises create by <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAll<TResult>(Func<IEnumerable<IPromise<TResult>>> promisesFactory) where TResult : class;

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAny(params IPromise[] promises);

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAny(IEnumerable<IPromise> promises);

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAny(Func<IEnumerable<IPromise>> promisesFactory);

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promises">
		/// An enumerable providing the promises (of type <seealso cref="IPromise{TResult}"/>) to wait for
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises in <paramref name="promises"/>
		/// </returns>
		IPromise ThenAny<TResult>(IEnumerable<IPromise<TResult>> promises) where TResult : class;

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to wait for which
		/// will be <seealso cref="IPromise{TResult}"/>
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAny<TResult>(Func<IEnumerable<IPromise<TResult>>> promisesFactory) where TResult : class;
	}

	/// <summary>
	/// A promise representing a future result of a specific type
	/// </summary>
	public interface IPromise<T> : IPromise where T : class {
		/// <summary>
		/// Add a handler for successful delivery of the <c>IPromise{T}</c>
		/// </summary>
		/// <param name="successHandler">The handler</param>
		IPromise<T> Success(Action<T> successHandler);

		/// <summary>
		/// Add an 'always' handler called when the <c>IPromise{T}</c> is delivered
		/// </summary>
		/// <param name="alwaysHandler">The always handler</param>
		IPromise<T> Always(Action<IPromiseController<T>> alwaysHandler);

		/// <summary>
		/// Add a handler for a failed delivery of the <c>IPromise{T}</c> (including
		/// a canceled <c>IPromise{T}</c>)
		/// </summary>
		/// <param name="catchHandler">The exception handler</param>
		new IPromise<T> Catch(Action<Exception> catchHandler);

		/// <summary>
		/// Add a handler for a canceled <c>IPromise{T}</c>
		/// </summary>
		/// <param name="cancelHandler">the cancellation handler</param>
		new IPromise<T> Canceled(Action cancelHandler);

		/// <summary>
		/// Chain an <c>IPromise</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promiseFactory">A function that provides the promise to chain based 
		/// on the result of this promise</param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between the two original promises
		/// </returns>
		IPromise Then(Func<T, IPromise> promiseFactory);

		/// <summary>
		/// Chain an <c>IPromise{TOther}</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="converter">
		/// A function converts the result of this promise to <typeparamref name="TOther"/> 
		/// </param>
		/// <returns>
		/// An <c>IPromise{TOther}</c> representing the link between the two original promises
		/// </returns>
		IPromise<TOther> Then<TOther>(Func<T, TOther> converter) where TOther : class ;

		/// <summary>
		/// Chain an <c>IPromise{TOther}</c> to the successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="converter">
		/// A function converts the result of this promise to <typeparamref name="TOther"/> 
		/// </param>
		/// <returns>
		/// An <c>IPromise{TOther}</c> representing the link between the two original promises
		/// </returns>
		IPromise<TOther> Then<TOther>(Func<T, IPromise<TOther>> converter) where TOther : class;

		/// <summary>
		/// Chain a promise to successful delivery of a collection of promises, after the 
		/// successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to chain based 
		/// on the result of this promise
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAll(Func<T, IEnumerable<IPromise>> promisesFactory);

		/// <summary>
		/// Chain a promise to successful delivery of (any) one of a collection of promises, after the 
		/// successful delivery of this <c>IPromise{TResult}</c>
		/// </summary>
		/// <param name="promisesFactory">
		/// A factory method that acts as an enumerable providing the promises to chain based 
		/// on the result of this promise
		/// </param>
		/// <returns>
		/// An <c>IPromise</c> representing the link between this promise and the
		/// collection of promises created via <paramref name="promisesFactory"/>
		/// </returns>
		IPromise ThenAny(Func<T,IEnumerable<IPromise>> promisesFactory) ;
	}
}
