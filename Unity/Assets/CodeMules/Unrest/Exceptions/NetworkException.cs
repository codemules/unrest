﻿
using System ;

namespace CodeMules.Unrest {
    public class NetworkException : Exception {

        #region Constructor(s)

        public NetworkException() : this("An unexpected and unspecified network error occured.")
        {
        }

        public NetworkException(string errorMessage) : this(errorMessage, null)
        {
        }

        public NetworkException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {
        }

        public NetworkException(INetworkResponse networkResponse) : this(networkResponse.StatusMessage)
        {
            NetworkResponse = networkResponse;
        }

        #endregion

        #region Properties

        public bool HasNetworkResponse
        {
            get { return NetworkResponse != null; }
        }
        #endregion

        #region Methods

        public INetworkResponse NetworkResponse { get; private set; }

        #endregion
    }
}
