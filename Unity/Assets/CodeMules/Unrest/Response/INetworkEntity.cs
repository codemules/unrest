﻿
namespace CodeMules.Unrest {
	public interface INetworkEntity {

		byte[] Bytes { get; }

		string ContentType { get; }

		long ContentLength { get; }
	}
}
