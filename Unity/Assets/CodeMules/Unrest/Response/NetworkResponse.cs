﻿
using System ;
using System.Collections.Generic;
using System.Net;
using CodeMules.Utils;

namespace CodeMules.Unrest {

    public class NetworkResponse : INetworkResponse
    {

        #region Fields

        #endregion

        #region Constructor(s)

        public NetworkResponse(
            HttpStatusCode statusCode,
            INetworkEntity networkEntity,
            IDictionary<string, string> headers,
            string statusMessage = null,
            TimeSpan? duration = null
        )
        {
            StatusCode = statusCode;
            NetworkEntity = networkEntity ?? CodeMules.Unrest.NetworkEntity.Empty;
            StatusMessage = statusMessage ?? StatusCode.ToString();
            Headers = headers;
            Duration = duration;
        }

        #endregion

        #region Properties

        public IDictionary<string, string> Headers { get; private set; }

        public HttpStatusCode StatusCode { get; private set; }

        public string StatusMessage { get; private set; }

        public INetworkEntity NetworkEntity { get; private set; }

        public TimeSpan? Duration { get; private set; }

        #endregion

        #region Methods

        public string GetHeader(string key, string defaultValue)
        {
            return Headers.GetValue(key, defaultValue);
        }

        public T GetHeader<T>(string key, T defaultValue, Func<string, T> converter)
        {
            return Headers.GetValue<T>(key, defaultValue, converter);
        }

        #endregion
    }
}
