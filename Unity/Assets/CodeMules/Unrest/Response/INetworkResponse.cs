﻿using System;
using System.Collections.Generic;
using System.Net;
            
namespace CodeMules.Unrest {
	public interface INetworkResponse {

		HttpStatusCode StatusCode { get; }

		string StatusMessage { get; }

		INetworkEntity NetworkEntity { get; }

		IDictionary<string, string> Headers { get; }

		TimeSpan? Duration { get; }

		string GetHeader(string key,string defaultValue);

		T GetHeader<T>(string key, T defaultValue,Func<string,T> converter);
	}
}
