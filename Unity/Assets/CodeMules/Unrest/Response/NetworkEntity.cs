﻿using System ;
using System.Text;

namespace CodeMules.Unrest
{
    public class NetworkEntity : INetworkEntity
    {
        #region Constants
        /// <summary>
        /// An entity to use when the response doesn't have one
        /// </summary>
        public static readonly NetworkEntity Empty = new NetworkEntity(new byte [0],HttpContentType.OctetStreamContentType) ;
        #endregion

        #region Constructor(s)

        public NetworkEntity(byte[] bytes, string contentType) {
            Bytes = bytes;
            ContentType = contentType;
        }
        #endregion

        #region Properties

        public byte [] Bytes { get; private set;  }

        public string ContentType { get; private set; }

        public long ContentLength {
            get { return Bytes.Length; }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Convert the <c>NetworkEntity</c> to a <see cref="string"/>
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents the current <c>NetworkEntity</c>
        /// using the <c>charset</c> encoding specified in <see cref="ContentType"/>
        /// (or the default if charset is not specified.)
        /// </returns>
        public override string ToString() {
            if (ContentLength == 0) {
                return string.Empty;
            }

            string charSet = HttpContentType.ParseContentCharSet(ContentType);

            Encoding encoding = Encoding.GetEncoding(charSet);

            if (encoding == null) {
                throw new ArgumentException("Encoding Not Found for '{0}'", charSet);
            }

            return encoding.GetString(Bytes);
        }
        #endregion

    }
}
