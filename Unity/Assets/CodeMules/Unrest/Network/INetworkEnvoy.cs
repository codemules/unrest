﻿using System;
using CodeMules.Promises;

namespace CodeMules.Unrest
{
    public interface INetworkEnvoy
    {
        string Name { get; }

        Uri BaseUrl { get; }

        INetwork Network { get; }

        IPromise<INetworkResponse> Send(IRequest request);

        void Cancel(object groupId);

        void CancelAll();
    }
}