﻿using CodeMules.Promises;

namespace CodeMules.Unrest
{
    public interface INetwork
    {
        IPromise<INetworkResponse> SendRequest(INetworkRequestController networkRequestController);

        IResponseDelivery ResponseDelivery { get; }
    }
}