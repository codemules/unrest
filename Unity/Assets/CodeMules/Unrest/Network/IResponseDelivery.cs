﻿using System;

namespace CodeMules.Unrest
{
    public interface IResponseDelivery
    {
        void SendResponse(INetworkRequestController networkRequest, INetworkResponse networkResponse);
        void SendResponse(INetworkRequestController networkRequest, Exception ex);
    }
}