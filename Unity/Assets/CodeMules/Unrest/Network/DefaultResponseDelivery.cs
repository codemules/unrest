﻿using System;

namespace CodeMules.Unrest
{
    public class DefaultResponseDelivery : IResponseDelivery
    {
        public void SendResponse(INetworkRequestController networkRequest, INetworkResponse networkResponse)
        {
            networkRequest.Promise.Deliver(networkResponse);
            networkRequest.Deliver(networkResponse);
        }

        public void SendResponse(INetworkRequestController networkRequest, Exception ex)
        {
            networkRequest.Promise.Deliver(ex);
            networkRequest.Deliver(ex);
        }
    }
}