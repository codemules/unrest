﻿using System;
using CodeMules.Promises;

namespace CodeMules.Unrest
{
    public class NetworkEnvoy : INetworkEnvoy
    {
        public NetworkEnvoy(string name, INetwork network, string baseUrl = null)
            : this(name,network,(baseUrl != null) ? new Uri(baseUrl) : null)
        {
        }

        public NetworkEnvoy(string name, INetwork network, Uri baseUrl = null)
        {
            Name = name;
            Network = network;
            BaseUrl = baseUrl;
        }

        public string Name { get; private set; }

        public Uri BaseUrl { get; private set; }

        public INetwork Network { get; private set; }

        public IPromise<INetworkResponse> Send(IRequest request) {
            return Network.SendRequest(
                    CreateNetworkRequest(request,BaseUrl)
                    );
        }

        public virtual void Cancel(object groupId)
        {
        }

        public virtual void CancelAll()
        {
        }

        protected virtual INetworkRequestController CreateNetworkRequest(IRequest request, Uri baseUrl)
        {
            return new NetworkRequest(request, BaseUrl);
        }
    }
}