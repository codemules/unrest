﻿using System;
using System.Collections.Generic;

namespace CodeMules.Utils {
	public static class CollectionExtensions {

		#region Dictionary Extensions
	    public static IDictionary<TKey, TValue> MergeLeft<TKey, TValue>(
	        this IDictionary<TKey, TValue> target,
	        params IEnumerable<KeyValuePair<TKey, TValue>>[] others
	    ) {
	        foreach (var dict in others) {
	            if (dict == null)
	                continue;

	            foreach (var kvp in dict) {
	                if (!target.ContainsKey(kvp.Key)) {
	                    target[kvp.Key] = kvp.Value;
	                }
	            }
	        }

	        return target;
	    }

	    public static IDictionary<TKey, TValue> MergeLeft<TKey, TValue>(
			this IDictionary<TKey, TValue> target,
			params IDictionary<TKey, TValue>[] others
			) {
			foreach (var dict in others) {
				if (dict == null)
					continue;

				foreach (var kvp in dict) {
					if (!target.ContainsKey(kvp.Key)) {
						target[kvp.Key] = kvp.Value;
					}
				}
			}

			return target;
		}

		public static IDictionary<TKey, TValue> MergeRight<TKey, TValue>(
			this IDictionary<TKey, TValue> target,
			params IDictionary<TKey, TValue>[] others
			)
		{
			foreach (var dict in others) {
				if (dict == null)
					continue;

				foreach (var kvp in dict) {
					target[kvp.Key] = kvp.Value;
				}
			}

			return target;
		}

	    public static IDictionary<TKey, TValue> MergeRight<TKey, TValue>(
	        this IDictionary<TKey, TValue> target,
	        params IEnumerable<KeyValuePair<TKey, TValue>>[] others
	    )
	    {
	        foreach (var dict in others) {
	            if (dict == null)
	                continue;

	            foreach (var kvp in dict) {
	                target[kvp.Key] = kvp.Value;
	            }
	        }

	        return target;
	    }

	    public static T GetValue<T>(
			this IDictionary<string, object> dictionary, 
			string key, 
			T defaultValue, 
			Func<object, T> converter = null
			) 
		{
			object value;

			if (!dictionary.TryGetValue(key, out value)) {
				return defaultValue;
			}

		    // Note that implicit/explicit tpe conversion to T will not work here
		    // You must you a custom converter that does something more than casting
		    // value (typed as 'object') to T
			return (converter != null) ? converter(value) : (T)value;
		}

		public static T GetValue<T>(
			this IDictionary<string, string> dictionary, 
			string key, T defaultValue, 
			Func<string, T> converter 
			) 
		{
			string value;

			if (!dictionary.TryGetValue(key, out value)) {
				return defaultValue;
			}

			return converter(value);
		}

		public static string GetValue(
			this IDictionary<string, string> dictionary, 
			string key, 
			string defaultValue
			) 
		{
			string value;

			if (!dictionary.TryGetValue(key, out value)) {
				value = defaultValue;
			}

			return value;
		}

		#endregion
	}
}
