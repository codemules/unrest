﻿using System;

namespace CodeMules.Utils{
    /// <summary>
    /// Simple string named object/type
    /// </summary>
    /// <remarks>
    /// Provides implicit conversion to <see cref="string"/> and all comparisons
    /// are supported for both <c>NamedType</c> and <see cref="string"/> allowing
    /// for highly transparent usage, and ultimately, allowing a string value to
    /// be strongly typed.
    /// <para>
    /// Note that you cannot create instances of this class; you have to create your
    /// own type as that is the whole point.
    /// </para>
    /// </remarks>
    [Serializable]
    public class NamedType :
        IEquatable<string>,
        IComparable<string>,
        IEquatable<NamedType>,
        IComparable<NamedType>,
        IComparable
    {
        #region Static Operators
        /// <summary>
        /// Static operator for implicit <see cref="string"/> conversion
        /// </summary>
        /// <param name="namedType">The named type</param>
        /// <returns>
        /// <c>this.ToString()</c>
        /// </returns>
        public static implicit operator string(NamedType namedType) {
            return namedType.ToString();
        }

        /// <summary>
        /// Compare two <c>NamedType</c> instances for equality
        /// </summary>
        /// <param name="namedType1">The 1st named type</param>
        /// <param name="namedType2">The 2nd named type</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator==(
            NamedType namedType1,
            NamedType namedType2
        )
        {
            if (ReferenceEquals(namedType1,null) || ReferenceEquals(namedType2,null)) {
                return ReferenceEquals(namedType1, namedType2);
            }

            // must be of the same type
            if (namedType1.GetType() != namedType2.GetType()) {
                return false;
            }

            return string.Compare(
                       namedType1.Name,
                       namedType2.Name,
                       StringComparison.OrdinalIgnoreCase
                   ) == 0;
        }

        /// <summary>
        /// Compare two <c>NamedType</c> instances for inequality
        /// </summary>
        /// <param name="namedType1">The 1st named type</param>
        /// <param name="namedType2">The 2nd named type</param>
        /// <returns>The result of the inequality comparison</returns>
        public static bool operator!=(NamedType namedType1,NamedType namedType2) {
            return !(namedType1 == namedType2);
        }

        /// <summary>
        /// Compare a <c>NamedType</c> to a <see cref="string"/> for equality
        /// </summary>
        /// <param name="namedType">The named type</param>
        /// <param name="str">The string to compare</param>
        /// <returns>The result of the equality comparison</returns>
        public static bool operator==(NamedType namedType,string str) {
            if (ReferenceEquals(namedType,null) || ReferenceEquals(str,null)) {
                return false;
            }

            return string.Compare(
                       namedType.Name,
                       str,
                       StringComparison.OrdinalIgnoreCase
                   ) == 0;
        }

        /// <summary>
        /// Compare a <c>NamedType</c> to a <see cref="string"/> for inequality
        /// </summary>
        /// <param name="namedType">The named type</param>
        /// <param name="str">The string representation</param>
        /// <returns>
        /// <c>true</c> if <paramref name="namedType"/> is != to <paramref name="str"/>,
        /// <c>false</c> otherwise
        /// </returns>
        public static bool operator!=(NamedType namedType,string str) {
            return !(namedType == str);
        }
        #endregion Static Operators

        #region Constructor(s)
        /// <summary>
        /// Initializes a new instance of the <see cref="NamedType"/> class.
        /// </summary>
        /// <param name="name">The name of the <c>NamedType</c></param>
        protected NamedType(string name) {
            Name = name;
        }
        #endregion Constructor(s)

        #region Properties
        /// <summary>
        /// The name of the <see cref="NamedType"/>
        /// </summary>
        public string Name { get; private set; }
        #endregion Properties

        #region Equality Implementation
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code of the string representation of <see cref="Name"/> converted to canonical lower-case
        /// </returns>
        public override int GetHashCode() {
            return Name.ToLower().GetHashCode();
        }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings:
        /// &lt; 0    - this instance is less than <paramref name="obj"/>.
        /// == 0      - this instance is equal to <paramref name="obj"/>.
        /// &gt; 0    - this instance is greater than <paramref name="obj"/>.
        /// </returns>
        /// <param name="obj">
        /// An object to compare with this instance.
        /// </param>
        /// <exception cref="T:System.ArgumentException">
        /// <paramref name="obj"/> is not the same type as this instance.
        /// </exception>
        public int CompareTo(object obj) {
            if (ReferenceEquals(obj,null))
                return 1;

            string str = obj as string;

            if (str != null) {
                return CompareTo(str);
            }

            return CompareTo(obj as NamedType);
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj) {
            NamedType other = obj as NamedType;

            if (other != null) {
                return this == other ;
            }

            string str = obj as string;

            if (str == null) {
                return false;
            }

            return this == str ;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">
        /// A <see cref="string"/> to compare with this <c>NamedType</c>
        /// </param>
        public bool Equals(string other) {
            return this == other;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">
        /// A <c>NamedType</c> to compare with this <c>NamedType</c>
        /// </param>
        public bool Equals(NamedType other) {
            return this == other;
        }
        #endregion Equality Implementation

        #region Overrides
        /// <summary>
        /// Compares this <c>NamedType</c> with a <see cref="string"/>.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings:
        /// &lt; 0    - this instance is less than <paramref name="other"/>.
        /// == 0      - this instance is equal to <paramref name="other"/>.
        /// &gt; 0    - this instance is greater than <paramref name="other"/>.
        /// </returns>
        /// <param name="other">
        /// A <see cref="string"/> to compare with this <c>NamedType</c>
        /// </param>
        public int CompareTo(string other) {
            return string.Compare(Name, other, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Compares this <c>NamedType</c> with another instance of <c>NamedType</c>
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings:
        /// &lt; 0    - this instance is less than <paramref name="other"/>.
        /// == 0      - this instance is equal to <paramref name="other"/>.
        /// &gt; 0    - this instance is greater than <paramref name="other"/>.
        /// </returns>
        /// <param name="other">
        /// Another <c>NamedType</c> instance to compare with this <c>NamedType</c>.
        /// </param>
        public int CompareTo(NamedType other) {
            if (other == null)
                return 1 ;

            return string.Compare(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Returns a <see cref="string"/> representation of this <c>NamedType</c>
        /// </summary>
        /// <returns>
        /// a <see cref="string"/> representation of this <c>NamedType</c>
        /// </returns>
        public override string ToString() {
            return Name;
        }
        #endregion Overrides
    }
}

