﻿using System ;
using System.Collections.Generic;

using CodeMules.Utils;

namespace CodeMules.Unrest {
    /// <summary>
    /// Default implementation of <see cref="IRequest"/> and <see cref="IRequestCreator"/>
    /// </summary>
    public class Request : IRequestCreator {
        #region Fields

        private IDictionary<string, object> headers;
        private IDictionary<string, object> queryParams;
        private IDictionary<string, object> formParams;

        #endregion

        #region Static Helpers
        public static Request Get(string url) {
            return new Request(url,HttpMethod.Get);
        }

        public static Request Post(string url) {
            return new Request(url,HttpMethod.Post);
        }

        public static Request Put(string url) {
            return new Request(url,HttpMethod.Put);
        }

        public static Request Delete(string url) {
            return new Request(url,HttpMethod.Delete);
        }

        public static Request Head(string url) {
            return new Request(url,HttpMethod.Head);
        }
        #endregion

        #region Constructor(s)

        public Request(string url,HttpMethod httpMethod) {
            Url = new Uri(url,UriKind.RelativeOrAbsolute);
            HttpMethod = httpMethod;
        }

        public Request(Uri url,HttpMethod httpMethod) {
            Url = url;
            HttpMethod = httpMethod;
        }
        #endregion

        #region IRequest implementation

        public Uri Url { get; private set; }
        public HttpMethod HttpMethod { get; private set; }

        public string ContentType {
            get {
                return InternalHeaders.GetValue<string>(HttpHeader.ContentType,null,(obj) => obj.ToString()) ;
            }
        }

        public string Accepts {
            get { return InternalHeaders.GetValue<string>(HttpHeader.Accept, null,(obj) => obj.ToString()); }
        }

        public object GroupId { get; private set; }

        public byte[] Body { get; private set; }

        public Func<byte[]> BodyProvider { get; private set; }

        public bool HasHeaders
        {
            get { return (headers != null) && (headers.Count > 0); }
        }

        public bool HasQueryParams
        {
            get { return (queryParams != null) && (queryParams.Count > 0); }
        }

        public bool HasFormParams
        {
            get { return (formParams != null) && (formParams.Count > 0); }
        }

        public IEnumerable<KeyValuePair<string, object>> Headers
        {
            get { return InternalHeaders; }
        }

        public IEnumerable<KeyValuePair<string, object>> QueryParams
        {
            get { return InternalQueryParams; }
        }

        public IEnumerable<KeyValuePair<string, object>> FormParams
        {
            get { return InternalFormParams; }
        }

        public byte[] GetRequestBody() {
            if ((Body == null) && (BodyProvider != null))
            {
                Body = BodyProvider();
            }

            return Body;
        }
        #endregion

        #region IRequestCreator implementation
        public IRequestCreator WithBodyProvider(Func<byte[]> bodyProvider)
        {
            BodyProvider = bodyProvider;
            return this;
        }

        public IRequestCreator WithBody(byte[] body)
        {
            Body = body;
            return this;
        }

        public IRequestCreator WithContentType(string contentType)
        {
            InternalHeaders[HttpHeader.ContentType] = contentType;
            return this;
        }

        public IRequestCreator WithContentType(HttpContentType contentType)
        {
            return WithContentType((string) contentType);
        }

        public IRequestCreator WithAccept(string acceptType)
        {
            InternalHeaders[HttpHeader.Accept] = acceptType;
            return this;
        }

        public IRequestCreator WithAccept(HttpContentType acceptType)
        {
            return WithAccept((string) acceptType);
        }

        public IRequestCreator WithGroupId(object groupId)
        {
            GroupId = groupId;
            return this;
        }

        public IRequestCreator WithHeader(string key, object value)
        {
            if (value != null)
            {
                InternalHeaders[key] = value;
            }
            else
            {
                InternalHeaders.Remove(key);
            }

            return this;
        }

        public IRequestCreator WithHeader(KeyValuePair<string, object> kvp)
        {
            InternalHeaders[kvp.Key] = kvp.Value;
            return this;
        }

        public IRequestCreator WithHeaders(IEnumerable<KeyValuePair<string, object>> headers)
        {
            InternalHeaders.MergeRight(headers);
            return this;
        }

        public IRequestCreator WithQueryParam(string key, object value)
        {
            if (value != null)
            {
                InternalQueryParams[key] = value;
            }
            else
            {
                InternalQueryParams.Remove(key);
            }

            return this;
        }

        public IRequestCreator WithQueryParam(KeyValuePair<string, object> kvp)
        {
            InternalQueryParams[kvp.Key] = kvp.Value;
            return this;
        }

        public IRequestCreator WithQueryParams(IEnumerable<KeyValuePair<string, object>> queryParams)
        {
            InternalQueryParams.MergeRight(queryParams);
            return this;
        }

        public IRequestCreator WithFormParam(string key, object value)
        {
            if (value != null)
            {
                InternalFormParams[key] = value;
            }
            else
            {
                InternalFormParams.Remove(key);
            }

            return this;
        }

        public IRequestCreator WithFormParam(KeyValuePair<string, object> kvp)
        {
            InternalFormParams[kvp.Key] = kvp.Value;
            return this;
        }

        public IRequestCreator WithFormParams(IEnumerable<KeyValuePair<string, object>> formParams)
        {
            InternalFormParams.MergeRight(formParams);
            return this;
        }
        #endregion

        #region Implementation

        private IDictionary<string, object> InternalHeaders
        {
            get
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                }

                return headers;
            }
        }

        private IDictionary<string, object> InternalQueryParams
        {
            get
            {
                if (queryParams == null)
                {
                    queryParams = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                }

                return queryParams;
            }
        }

        private IDictionary<string, object> InternalFormParams
        {
            get
            {
                if (formParams == null)
                {
                    formParams = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                }

                return formParams;
            }
        }
        #endregion

        #region IDisposable implementation

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                headers = queryParams = formParams = null;
                Body = null;
                BodyProvider = null;
            }
        }

        public void Dispose() {
            Dispose(true);
        }
        #endregion
    }
}
