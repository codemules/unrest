﻿
using System ;
using System.Collections.Generic;
using CodeMules.Promises;
using CodeMules.Utils;

namespace CodeMules.Unrest {
    /// <summary>
    /// Default implementation of <see cref="INetworkRequest"/> and
    /// <see cref="INetworkRequestController"/>
    /// </summary>
    public class NetworkRequest : INetworkRequestController {
        #region Fields

        private IPromiseController<INetworkResponse> promise;
        private IDictionary<string, object> headers;

        #endregion

        #region Constructor(s)

        public NetworkRequest(IRequest request, string baseUrl = null)
            : this(request,(baseUrl != null) ? new Uri(baseUrl) : null)
        {
        }

        public NetworkRequest(IRequest request,Uri baseUrl = null)
        {
            Request = request;
            WhenCreated = DateTime.UtcNow;
            RequestId = Guid.NewGuid().ToString();
            BaseUrl = baseUrl ;
            OriginUrl = CreateOriginUri(request, BaseUrl);
        }

        #endregion

        #region INetworkRequest implementation

        public IRequest Request { get; private set; }
        public string RequestId { get; private set; }
        public ulong SequenceId { get; private set; }
        public DateTime WhenCreated { get; private set; }
        public DateTime? WhenProcessed { get; set; }
        public DateTime? WhenCompleted { get; set; }

        public Uri BaseUrl { get; private set; }

        public Uri OriginUrl { get; private set; }

        public bool HasHeaders
        {
            get { return (headers != null) && (headers.Count > 0); }
        }

        public IEnumerable<KeyValuePair<string, object>> Headers
        {
            get { return InternalHeaders; }
        }

        public INetworkRequest WithHeader(string key, object value)
        {
            if (key == null)
            {
                InternalHeaders.Remove(key);
            }
            else
            {
                InternalHeaders[key] = value;
            }
            return this;
        }

        public INetworkRequest WithHeader(KeyValuePair<string, object> kvp)
        {
            InternalHeaders[kvp.Key] = kvp.Value;
            return this;
        }

        public INetworkRequest WithHeaders(IEnumerable<KeyValuePair<string, object>> headers)
        {
            InternalHeaders.MergeRight(headers);
            return this;
        }
        #endregion

        #region INetworkRequestController implementation
        public Uri EndpointUrl
        {
            get { return RedirectUrl ?? OriginUrl; }
        }

        public Uri RedirectUrl { get; private set; }

        public bool IsDelivered { get; private set; }

        public INetworkResponse Response { get; private set; }

        public Exception Error { get; set; }

        public IPromiseController<INetworkResponse> Promise
        {
            get
            {
                if (promise == null)
                {
                    promise = new Promise<INetworkResponse>();
                }

                return promise;
            }
        }


        public void Deliver(INetworkResponse networkResponse)
        {
            IsDelivered = true;
            Response = networkResponse;
            WhenCompleted = DateTime.UtcNow;
        }

        public void Deliver(Exception exception)
        {
            IsDelivered = true;
            Error = exception;
            WhenCompleted = DateTime.UtcNow;
        }

        public void SetRedirectUrl(string redirectUrl)
        {
            Uri redirectUri = new Uri(redirectUrl);

            if (Request.HasQueryParams)
            {
                AddQueryStringToUri(Request, redirectUri);
            }

            RedirectUrl = redirectUri;
        }

        #endregion

        #region Implementation

        private IDictionary<string, object> InternalHeaders
        {
            get
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                }

                return headers;
            }
        }

        private Uri CreateOriginUri(IRequest request, Uri baseUri)
        {
            Uri result;

            if (baseUri == null)
            {
                // request specifies full url
                result = request.Url;

                if (result == null)
                {
                    throw new ArgumentException("Expecting request to provide a Url", "request");
                }
            }
            else if (request.Url == null)
            {
                // baseUrl specifies full url
                result = baseUri;

                // When we have both the base and the request url check for relative url
            }
            else if (!request.Url.IsAbsoluteUri)
            {
                result = new Uri(baseUri, request.Url);
            }
            else
            {
                // if request.Url is a superset of the base url, then use it
                if (baseUri.IsBaseOf(request.Url))
                {
                    result = request.Url;
                }
                else
                {
                    throw new UriFormatException(
                        "Request Url is either unexpected (not-null), not relative to the base url, or not a superset of the base url"
                    );
                }
            }

            if (request.HasQueryParams)
            {
                result = AddQueryStringToUri(request, result);
            }

            return result;
        }

        private static Uri AddQueryStringToUri(IRequest request, Uri targetUri)
        {
            Uri result = targetUri;

            if (request.HasQueryParams)
            {
                string query = HttpUtils.CreateQueryString(request.QueryParams);

                if (!String.IsNullOrEmpty(query))
                {
                    result = new Uri(string.Format("{0}{1}", result, query));
                }
            }

            return result;
        }

        #endregion
    }
}
