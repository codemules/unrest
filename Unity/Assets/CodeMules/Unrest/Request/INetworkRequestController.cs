﻿
using System ;
using CodeMules.Promises;

namespace CodeMules.Unrest {
    public interface INetworkRequestController : INetworkRequest {
        #region Properties

        bool IsDelivered { get; }

        INetworkResponse Response { get; }

        IPromiseController<INetworkResponse> Promise { get; }

        Uri EndpointUrl { get; }

        Uri RedirectUrl { get; }

        #endregion

        #region Methods

        void Deliver(INetworkResponse networkResponse);

        void Deliver(Exception exception);

        void SetRedirectUrl(string url);

        #endregion
    }
}
