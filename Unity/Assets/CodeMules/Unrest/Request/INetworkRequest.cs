﻿using System;
using System.Collections.Generic;

namespace CodeMules.Unrest {
	public interface INetworkRequest {

		IRequest Request { get; }

	    string RequestId { get; }

	    ulong SequenceId { get; }

		DateTime WhenCreated { get; }

		DateTime? WhenProcessed { get; set; }

		DateTime? WhenCompleted { get; set; }

	    Uri BaseUrl { get; }
		Uri OriginUrl { get; }

	    bool HasHeaders { get; }
	    IEnumerable<KeyValuePair<string, object>> Headers { get; }

	    INetworkRequest WithHeader(string key, object value);
	    INetworkRequest WithHeader(KeyValuePair<string,object> kvp);
	    INetworkRequest WithHeaders(IEnumerable<KeyValuePair<string,object>> headers);
	}
}
