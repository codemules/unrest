﻿using System;
using System.Collections.Generic;

namespace CodeMules.Unrest {

    /// <summary>
    /// A request template. Describes the request but does not contain any mutable date reflecting
    /// the state of an actual in-flight network request (see <see cref="INetworkRequest"/>)
    /// </summary>
    /// <remarks>
    /// Note that this interface is immutable by design and should be used whenever the mutable
    /// <see cref="IRequestCreator"/> is not needed. This allows instances of <c>IRequest</c> to
    /// act as templates, and thus, be re-used
    /// </remarks>
	public interface IRequest : IDisposable {

		Uri Url { get; }

		HttpMethod HttpMethod { get; }

		string ContentType { get; }
        string Accepts { get;  }

		object GroupId { get; }

		byte[] Body { get; }

		Func<byte[]> BodyProvider { get; }

	    byte[] GetRequestBody();

	    bool HasHeaders { get;  }
	    bool HasQueryParams { get ;  }
	    bool HasFormParams { get;  }

	    IEnumerable<KeyValuePair<string, object>> Headers { get; }
	    IEnumerable<KeyValuePair<string, object>> QueryParams { get; }
	    IEnumerable<KeyValuePair<string, object>> FormParams { get; }
	}
}
