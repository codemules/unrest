﻿using System;
using System.Collections.Generic;

namespace CodeMules.Unrest
{
    /// <summary>
    /// This interface provides a mutable version of <see cref="IRequest"/>
    /// </summary>
    public interface IRequestCreator : IRequest
    {
        IRequestCreator WithBodyProvider(Func<byte[]> bodyProvider);
        IRequestCreator WithBody(byte[] body);

        IRequestCreator WithGroupId(object groupId);

        IRequestCreator WithContentType(HttpContentType contentType);
        IRequestCreator WithContentType(string contentType);

        IRequestCreator WithAccept(HttpContentType acceptType);
        IRequestCreator WithAccept(string acceptType);

        IRequestCreator WithHeader(string key, object value);
        IRequestCreator WithHeader(KeyValuePair<string,object> kvp);
        IRequestCreator WithHeaders(IEnumerable<KeyValuePair<string,object>> headers);

        IRequestCreator WithQueryParam(string key, object value);
        IRequestCreator WithQueryParam(KeyValuePair<string, object> kvp);
        IRequestCreator WithQueryParams(IEnumerable<KeyValuePair<string, object>> queryParams);

        IRequestCreator WithFormParam(string key, object value);
        IRequestCreator WithFormParam(KeyValuePair<string, object> kvp);
        IRequestCreator WithFormParams(IEnumerable<KeyValuePair<string, object>> formParams);
    }
}