﻿using System;
using CodeMules.Utils;

namespace CodeMules.Unrest
{
    public class HttpContentType : NamedType
    {
        public static readonly HttpContentType JsonContentType= new HttpContentType("application/json");
        public static readonly HttpContentType JsonUtfContentType= new HttpContentType("application/json; charset=utf-8");
        public static readonly HttpContentType FormEncodingContentType = new HttpContentType("application/x-www-form-urlencoded");
        public static readonly HttpContentType OctetStreamContentType = new HttpContentType("application/octet-stream");
        public static readonly HttpContentType TextContentType = new HttpContentType("text/plain");
        public static readonly HttpContentType HtmlContentType = new HttpContentType("text/html");
        public static readonly HttpContentType XmlContentType = new HttpContentType("application/xml");

        public const string CharSetAttribute = "charset";

        public const string Utf8CharSet = "UTF-8";
        public const string AsciiCharSet = "us-ascii";
        public const string LatinAlphabet1CharSet = "ISO-8859-1";
        public const string LatinHebrewCharSet = "ISO-8859-8";


        public HttpContentType(string contentType) : base(contentType)
        {
        }

        public static string FormatContentType(string contentType, string charset = Utf8CharSet) {
            return string.Format(
                "{0}; {1}={2}",
                contentType,
                CharSetAttribute,
                charset
            );
        }

        public static string ParseContentCharSet(string contentTypeHeader, string defaultCharSet = Utf8CharSet) {
            string result = null;

            // separate content-type from charset
            string[] parts = contentTypeHeader.Split(';');

            for (int i = 1; i < parts.Length; i++) {
                // split key/value
                parts = parts[1].Split('=');

                if ((parts.Length == 2) &&
                    (string.Compare(parts[0], CharSetAttribute, StringComparison.InvariantCultureIgnoreCase) == 0)) {
                    // result is the value of 'charset=value'
                    result = parts[1];
                }
            }

            return result ?? defaultCharSet;
        }

        public static string ParseContentType(string contentTypeHeader) {
            string result = null ;

            int index = contentTypeHeader.IndexOf(';');

            if (index > 0) {
                result = contentTypeHeader.Substring(0, index);
            }

            return result ?? contentTypeHeader ;
        }
    }
}