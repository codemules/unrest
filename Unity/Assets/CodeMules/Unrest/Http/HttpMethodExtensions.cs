using System.Net;

namespace CodeMules.Unrest
{
    public static class HttpMethodExtensions
    {
        public static bool HasRequestBody(this HttpMethod method) {
            return (method == HttpMethod.Patch) || (method == HttpMethod.Post) ||
                   (method == HttpMethod.Put);
        }

        public static bool IsCacheable(this HttpMethod method) {
            return (method == HttpMethod.Get) || (method == HttpMethod.Head);
        }

        public static bool HasResponseBody(this HttpMethod method, HttpStatusCode statusCode = HttpStatusCode.OK) {
            return (method != HttpMethod.Head) &&
                   !((HttpStatusCode.Continue <= statusCode) && (statusCode < HttpStatusCode.OK)) &&
                   (statusCode != HttpStatusCode.NoContent) &&
                   (statusCode != HttpStatusCode.NotModified);
        }
    }
}