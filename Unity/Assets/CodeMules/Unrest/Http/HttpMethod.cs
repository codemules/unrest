using CodeMules.Utils;

namespace CodeMules.Unrest {
    public class HttpMethod : NamedType {
        public static readonly HttpMethod Get = new HttpMethod("GET");
        public static readonly HttpMethod Post = new HttpMethod("POST");
        public static readonly HttpMethod Put = new HttpMethod("PUT");
        public static readonly HttpMethod Delete = new HttpMethod("DELETE");
        public static readonly HttpMethod Head = new HttpMethod("HEAD");
        public static readonly HttpMethod Options = new HttpMethod("OPTIONS");
        public static readonly HttpMethod Trace = new HttpMethod("TRACE");
        public static readonly HttpMethod Patch = new HttpMethod("PATCH");

        private HttpMethod(string name) : base(name) {}
    }
}
