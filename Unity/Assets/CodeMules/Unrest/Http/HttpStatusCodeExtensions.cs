﻿using System.Net;

namespace CodeMules.Unrest
{
    public static class HttpStatusCodeExtensions
    {
        public static bool IsNotModified(this HttpStatusCode statusCode) {
            return statusCode == HttpStatusCode.NotModified;
        }

        public static bool IsNotOkRange(this HttpStatusCode statusCode) {
            return (statusCode < HttpStatusCode.OK) || (statusCode > (HttpStatusCode)299);
        }

        public static bool IsRedirect(this HttpStatusCode statusCode) {
            return (statusCode == HttpStatusCode.MovedPermanently) ||
                   // REVIEW: This is old-style (302) where a POST is changed to a GET.
                   (statusCode == HttpStatusCode.Found) ||
                   (statusCode == HttpStatusCode.TemporaryRedirect);
        }
    }
}