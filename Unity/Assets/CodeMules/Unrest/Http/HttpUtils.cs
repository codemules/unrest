﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CodeMules.Unrest {
	public static class HttpUtils {

	    public static string CreateQueryString(IEnumerable<KeyValuePair<string, object>> queryParams) {
			StringBuilder sb = new StringBuilder();

			foreach (var kvp in queryParams) {
				if (sb.Length == 0)
					sb.Append('?');
				else
					sb.Append('&');

				sb.AppendFormat(
					"{0}={1}",
					Uri.EscapeDataString(kvp.Key),
					Uri.EscapeDataString(kvp.Value.ToString())
					);
			}

			return sb.ToString();
		}

		public static string CreateFormBody(
								IDictionary<string, object> formParams,
								Func<IDictionary, string> jsonSerializer
								) 
		{
			StringBuilder sb = new StringBuilder();

			foreach (var kvp in formParams) {
				if (sb.Length > 0) {
					sb.Append('&');
				}

				sb.AppendFormat("{0}=", Uri.EscapeDataString(kvp.Key));

				if (kvp.Value == null)
					continue;

				IDictionary dictValue = kvp.Value as IDictionary;

				if (dictValue != null) {
					sb.AppendFormat("{0}", Uri.EscapeDataString(jsonSerializer(dictValue)));
				} else {
					sb.AppendFormat("{0}", Uri.EscapeDataString(kvp.Value.ToString()));
				}
			}

			return sb.ToString();
		}

		public static DateTime? ParseServerDate(string date) {
			DateTime? result;

			try {
				result = DateTime.ParseExact(
									date,
									"ddd, dd MMM yyyy HH:mm:ss 'GMT'",
									CultureInfo.InvariantCulture.DateTimeFormat,
									DateTimeStyles.AssumeUniversal
								);
			} catch (Exception) {
				result = null;
			}

			return result;
		}

	    public static string GetBasicAuthPayload(string username, string password) {
	        return Base64Encode(string.Format("{0}:{1}", username, password));
	    }

	    public static string Base64Encode(string plainText) {
	        var plainTextBytes = Encoding.GetEncoding(HttpContentType.LatinAlphabet1CharSet).GetBytes(plainText);
	        return Convert.ToBase64String(plainTextBytes);
	    }

	    public static string Base64Decode(string base64EncodedData) {
	        var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
	        return Encoding.GetEncoding(HttpContentType.LatinAlphabet1CharSet).GetString(base64EncodedBytes);
	    }
	}
}
