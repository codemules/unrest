﻿using System;
using CodeMules.Utils;

namespace CodeMules.Unrest
{
    public class HttpContentEncoding : NamedType
    {
        public static readonly HttpContentEncoding GZip = new HttpContentEncoding("gzip");
        public static readonly HttpContentEncoding Deflate = new HttpContentEncoding("deflate");

        public HttpContentEncoding(string contentEncoding) : base(contentEncoding)
        {
        }

        public static bool IsEncodedAs(string contentEncodingHeader, string encoding) {
            try {
                // separate content-type from charset
                string[] parts = contentEncodingHeader.Split(';');

                foreach (var part in parts) {
                    if (string.Compare(part, encoding, StringComparison.InvariantCultureIgnoreCase) == 0)
                        return true;
                }

            } catch {

            }

            return false;
        }


        public static bool IsGzipEncoded(string contentEncodingHeader) {
            return IsEncodedAs(contentEncodingHeader, GZip);
        }

        public static bool IsDeflateEncoded(string contentEncodingHeader) {
            return IsEncodedAs(contentEncodingHeader, Deflate);
        }

    }
}