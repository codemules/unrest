﻿using CodeMules.Utils;

namespace CodeMules.Unrest {
    public class HttpHeader : NamedType {
        public static readonly HttpHeader Accept = new HttpHeader("Accept");
        public static readonly HttpHeader Authorization = new HttpHeader("Authorization");
        public static readonly HttpHeader CacheControl = new HttpHeader("Cache-Control");
        public static readonly HttpHeader Connection = new HttpHeader("Connection") ;
        public static readonly HttpHeader ContentEncoding = new HttpHeader("Content-Encoding") ;
        public static readonly HttpHeader ContentType = new HttpHeader("Content-Type") ;
        public static readonly HttpHeader Date = new HttpHeader("Date") ;
        public static readonly HttpHeader ETag = new HttpHeader("ETag") ;
        public static readonly HttpHeader Expires = new HttpHeader("Expires");
        public static readonly HttpHeader IfNoneMatch = new HttpHeader("If-None-Match");
        public static readonly HttpHeader IfModifiedSince = new HttpHeader("If-Modified-Since") ;
        public static readonly HttpHeader KeepAlive = new HttpHeader("keep-alive") ;
        public static readonly HttpHeader LastModified = new HttpHeader("Last-Modified");
        public static readonly HttpHeader RedirectLocation = new HttpHeader("Location");

        public HttpHeader(string header) : base(header) {}
    }
}