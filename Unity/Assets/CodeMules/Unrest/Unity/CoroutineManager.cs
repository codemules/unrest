﻿using UnityEngine;

namespace CodeMules.Unrest.Unity
{
    public static class CoroutineManager {
        private static GameObject coroutineGo ;

        private static GameObject CoroutineGO {
            get {
                if (coroutineGo == null) {
                    coroutineGo = CreateGameObject("Unrest.Coroutines");
                    Object.DontDestroyOnLoad(coroutineGo);
                }

                return coroutineGo ;
            }
        }

        private static GameObject CreateGameObject(string name,Transform parent = null) {
            GameObject result = new GameObject(name) ;

            result.transform.localPosition = Vector3.zero ;
            result.transform.localRotation = Quaternion.identity ;
            result.transform.localScale = Vector3.one ;
            result.transform.parent = parent ;

            return result ;
        }

        public static CoroutineRunnerBehaviour CreateCoroutineRunner(string name) {
            GameObject go = CreateGameObject(name,CoroutineGO.transform);
            return go.AddComponent<CoroutineRunnerBehaviour>();
        }

        public static void RemoveCoroutineRunner(string name) {
            Transform  transform = CoroutineGO.transform.FindChild(name);

            GameObject go = (transform != null) ? transform.gameObject : null;

            if (go != null) {
                Object.Destroy(go);
            }
        }
    }

    public class CoroutineRunnerBehaviour : MonoBehaviour {}
}