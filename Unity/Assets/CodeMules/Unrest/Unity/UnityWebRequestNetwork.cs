﻿using UnityEngine;
using UnityEngine.Networking;

using System ;
using System.Collections;
using System.Collections.Generic;
using System.Net ;
using CodeMules.Promises;


namespace CodeMules.Unrest.Unity
{
    /// <summary>
    /// Implementation of <see cref="INetwork"/> that uses <see cref="UnityWebRequest"/>
    /// </summary>
    public class UnityWebRequestNetwork : INetwork
    {
        private readonly CoroutineRunnerBehaviour crb;

        #region Constructor(s)
        public UnityWebRequestNetwork(string networkName,IResponseDelivery responseDelivery = null) {
            crb = CoroutineManager.CreateCoroutineRunner(networkName);
            ResponseDelivery = responseDelivery ?? new DefaultResponseDelivery();
        }
        #endregion

        #region INetwork implementation
        public IResponseDelivery ResponseDelivery { get; private set; }

        public IPromise<INetworkResponse> SendRequest(INetworkRequestController networkRequestController)
        {
            crb.StartCoroutine(ProcessNetworkRequest(networkRequestController));
            return networkRequestController.Promise;
        }
        #endregion

        #region Implementation

        private static readonly HashSet<string> ReservedHeaderKeys = new HashSet<string>()
        {
            "accept-charset",
            "access-control-request-headers",
            "access-control-request-method",
            "connection",
            "content-length",
            "date",
            "dnt",
            "expect",
            "host",
            "keep-alive",
            "origin",
            "referer",
            "te",
            "trailer",
            "transfer-encoding",
            "upgrade",
            "user-agent",
            "via",
            "x-unity-version",

#if UNITY_WEBGL
            , "cookie"
            , "cookie2"
#endif
        };

        public IEnumerator ProcessNetworkRequest(INetworkRequestController networkRequest)
        {
            while (true)
            {
                UnityWebRequest unityWebRequest = CreateWebRequest(networkRequest);

                yield return unityWebRequest.Send();

                HttpStatusCode statusCode = (HttpStatusCode) ((int) unityWebRequest.responseCode) ;

                if (statusCode.IsRedirect())
                {
                    networkRequest.SetRedirectUrl(unityWebRequest.GetResponseHeader(HttpHeader.RedirectLocation));
                    Debug.Log("Redirect => " + networkRequest.EndpointUrl);
                    continue;
                }

                if (unityWebRequest.isError) {
                    Debug.Log(unityWebRequest.responseCode);
                    ResponseDelivery.SendResponse(networkRequest,new NetworkException(unityWebRequest.error));
                    unityWebRequest.Dispose();
                    yield break ;
                }

                try {
                    INetworkResponse networkResponse = CreateNetworkResponse(
                        networkRequest,
                        unityWebRequest
                    );

                    unityWebRequest.Dispose();

                    // if it is NOT a 304 or NOT in the 2xx range then we throw
                    // and handle in the catch below
                    if (!networkResponse.StatusCode.IsNotModified() && networkResponse.StatusCode.IsNotOkRange()) {
                        // throw an exception to ourselves
                        throw new NetworkException(networkResponse);
                    }

                    // Deliver the response
                    ResponseDelivery.SendResponse(networkRequest,networkResponse);

                    yield break ;
                } catch(NetworkException nex) {
                    if (nex.NetworkResponse != null) {
                        if (nex.NetworkResponse.StatusCode.IsRedirect())
                        {
                            string redirectUrl;
                            nex.NetworkResponse.Headers.TryGetValue(HttpHeader.RedirectLocation, out redirectUrl);
                            networkRequest.SetRedirectUrl(redirectUrl);

                        } else if (!ShouldAttemptRetry(nex))
                        {
                            ResponseDelivery.SendResponse(networkRequest,nex);
                            yield break ;
                        }
                    }
                    else
                    {
                        ResponseDelivery.SendResponse(networkRequest,nex);
                        yield break ;
                    }
                } catch(Exception ex)
                {
                    ResponseDelivery.SendResponse(networkRequest,new NetworkException("Unspecified network error", ex));
                    yield break ;
                }
            }
        }

        private INetworkResponse CreateNetworkResponse(
            INetworkRequestController networkRequest,
            UnityWebRequest webResponse,
            TimeSpan? duration = null
            )
        {
            HttpStatusCode statusCode = (HttpStatusCode) ((int) webResponse.responseCode);
            IDictionary<string, string> headers = webResponse.GetResponseHeaders();

            INetworkEntity entity = null ;

            if (networkRequest.Request.HttpMethod.HasResponseBody(statusCode))
            {
                entity = new NetworkEntity(
                    webResponse.downloadHandler.data,
                    webResponse.GetResponseHeader(HttpHeader.ContentType)
                );
            }

            return new NetworkResponse(
                statusCode,
                entity,
                headers,
                null,
                duration
            );
        }

        private UnityWebRequest CreateWebRequest(INetworkRequestController networkRequest)
        {
            UnityWebRequest unityWebRequest = new UnityWebRequest(
                networkRequest.EndpointUrl.ToString(),
                networkRequest.Request.HttpMethod
            );

            unityWebRequest.redirectLimit = 0;

            // The networkRequest.Request is lower precedence so add headers 1st
            if (networkRequest.Request.HasHeaders) {
                foreach (var kvp in networkRequest.Request.Headers) {
                    if (!ReservedHeaderKeys.Contains(kvp.Key)) {
                        unityWebRequest.SetRequestHeader(kvp.Key, kvp.Value.ToString());
                    }
                }
            }

            // networkRequest headers have priority so overwrite anything duplicated
            // with networkRequest.Request
            if (networkRequest.HasHeaders) {
                foreach (var kvp in networkRequest.Headers) {
                    if (!ReservedHeaderKeys.Contains(kvp.Key)) {
                        unityWebRequest.SetRequestHeader(kvp.Key, kvp.Value.ToString());
                    }
                }
            }

            if (networkRequest.Request.HttpMethod.HasRequestBody())
            {
                byte[] body = networkRequest.Request.GetRequestBody();

                if (body != null) {
                    unityWebRequest.uploadHandler =
                        new UploadHandlerRaw(body) {
                            contentType = networkRequest.Request.ContentType
                        };
                    unityWebRequest.disposeUploadHandlerOnDispose = true;
                }
            }

            if (networkRequest.Request.HttpMethod.HasResponseBody())
            {
                unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
                unityWebRequest.disposeDownloadHandlerOnDispose = true;
            }

            return unityWebRequest;
        }

        private bool ShouldAttemptRetry(NetworkException nex) {
            return false;
        }

        #endregion
    }


}
